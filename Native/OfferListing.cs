namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class OfferListing : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string offerListingIdField;

		private Price priceField;

		private Price salePriceField;

		private Price amountSavedField;

		private string percentageSavedField;

		private string availabilityField;

		private OfferListingAvailabilityAttributes availabilityAttributesField;

		private bool isEligibleForSuperSaverShippingField;

		private bool isEligibleForSuperSaverShippingFieldSpecified;

		private bool isEligibleForPrimeField;

		private bool isEligibleForPrimeFieldSpecified;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string OfferListingId
		{
			get
			{
				return this.offerListingIdField;
			}
			set
			{
				this.offerListingIdField = value;
				this.RaisePropertyChanged( "OfferListingId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Price Price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
				this.RaisePropertyChanged( "Price" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public Price SalePrice
		{
			get
			{
				return this.salePriceField;
			}
			set
			{
				this.salePriceField = value;
				this.RaisePropertyChanged( "SalePrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public Price AmountSaved
		{
			get
			{
				return this.amountSavedField;
			}
			set
			{
				this.amountSavedField = value;
				this.RaisePropertyChanged( "AmountSaved" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 4 )]
		public string PercentageSaved
		{
			get
			{
				return this.percentageSavedField;
			}
			set
			{
				this.percentageSavedField = value;
				this.RaisePropertyChanged( "PercentageSaved" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string Availability
		{
			get
			{
				return this.availabilityField;
			}
			set
			{
				this.availabilityField = value;
				this.RaisePropertyChanged( "Availability" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public OfferListingAvailabilityAttributes AvailabilityAttributes
		{
			get
			{
				return this.availabilityAttributesField;
			}
			set
			{
				this.availabilityAttributesField = value;
				this.RaisePropertyChanged( "AvailabilityAttributes" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public bool IsEligibleForSuperSaverShipping
		{
			get
			{
				return this.isEligibleForSuperSaverShippingField;
			}
			set
			{
				this.isEligibleForSuperSaverShippingField = value;
				this.RaisePropertyChanged( "IsEligibleForSuperSaverShipping" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsEligibleForSuperSaverShippingSpecified
		{
			get
			{
				return this.isEligibleForSuperSaverShippingFieldSpecified;
			}
			set
			{
				this.isEligibleForSuperSaverShippingFieldSpecified = value;
				this.RaisePropertyChanged( "IsEligibleForSuperSaverShippingSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public bool IsEligibleForPrime
		{
			get
			{
				return this.isEligibleForPrimeField;
			}
			set
			{
				this.isEligibleForPrimeField = value;
				this.RaisePropertyChanged( "IsEligibleForPrime" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsEligibleForPrimeSpecified
		{
			get
			{
				return this.isEligibleForPrimeFieldSpecified;
			}
			set
			{
				this.isEligibleForPrimeFieldSpecified = value;
				this.RaisePropertyChanged( "IsEligibleForPrimeSpecified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}