namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class ItemAttributes : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string[] actorField;

		private string[] artistField;

		private string aspectRatioField;

		private string audienceRatingField;

		private string[] audioFormatField;

		private string[] authorField;

		private string bindingField;

		private string brandField;

		private string[] catalogNumberListField;

		private string[] categoryField;

		private string cEROAgeRatingField;

		private string clothingSizeField;

		private string colorField;

		private ItemAttributesCreator[] creatorField;

		private string departmentField;

		private string[] directorField;

		private string eANField;

		private string[] eANListField;

		private string editionField;

		private string[] eISBNField;

		private string episodeSequenceField;

		private string eSRBAgeRatingField;

		private string[] featureField;

		private string[] formatField;

		private string genreField;

		private string hardwarePlatformField;

		private string hazardousMaterialTypeField;

		private bool isAdultProductField;

		private bool isAdultProductFieldSpecified;

		private bool isAutographedField;

		private bool isAutographedFieldSpecified;

		private string iSBNField;

		private bool isEligibleForTradeInField;

		private bool isEligibleForTradeInFieldSpecified;

		private bool isMemorabiliaField;

		private bool isMemorabiliaFieldSpecified;

		private string issuesPerYearField;

		private ItemAttributesItemDimensions itemDimensionsField;

		private string itemPartNumberField;

		private string labelField;

		private ItemAttributesLanguage[] languagesField;

		private string legalDisclaimerField;

		private Price listPriceField;

		private string magazineTypeField;

		private string manufacturerField;

		private DecimalWithUnits manufacturerMaximumAgeField;

		private DecimalWithUnits manufacturerMinimumAgeField;

		private string manufacturerPartsWarrantyDescriptionField;

		private string mediaTypeField;

		private string modelField;

		private string modelYearField;

		private string mPNField;

		private string numberOfDiscsField;

		private string numberOfIssuesField;

		private string numberOfItemsField;

		private string numberOfPagesField;

		private string numberOfTracksField;

		private string operatingSystemField;

		private ItemAttributesPackageDimensions packageDimensionsField;

		private string packageQuantityField;

		private string partNumberField;

		private string[] pictureFormatField;

		private string[] platformField;

		private string productGroupField;

		private string productTypeNameField;

		private string productTypeSubcategoryField;

		private string publicationDateField;

		private string publisherField;

		private string regionCodeField;

		private string releaseDateField;

		private DecimalWithUnits runningTimeField;

		private string seikodoProductCodeField;

		private string sizeField;

		private string sKUField;

		private string studioField;

		private NonNegativeIntegerWithUnits subscriptionLengthField;

		private string titleField;

		private string trackSequenceField;

		private Price tradeInValueField;

		private string uPCField;

		private string[] uPCListField;

		private string warrantyField;

		private Price wEEETaxValueField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Actor", Order = 0 )]
		public string[] Actor
		{
			get
			{
				return this.actorField;
			}
			set
			{
				this.actorField = value;
				this.RaisePropertyChanged( "Actor" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Artist", Order = 1 )]
		public string[] Artist
		{
			get
			{
				return this.artistField;
			}
			set
			{
				this.artistField = value;
				this.RaisePropertyChanged( "Artist" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string AspectRatio
		{
			get
			{
				return this.aspectRatioField;
			}
			set
			{
				this.aspectRatioField = value;
				this.RaisePropertyChanged( "AspectRatio" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string AudienceRating
		{
			get
			{
				return this.audienceRatingField;
			}
			set
			{
				this.audienceRatingField = value;
				this.RaisePropertyChanged( "AudienceRating" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "AudioFormat", Order = 4 )]
		public string[] AudioFormat
		{
			get
			{
				return this.audioFormatField;
			}
			set
			{
				this.audioFormatField = value;
				this.RaisePropertyChanged( "AudioFormat" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Author", Order = 5 )]
		public string[] Author
		{
			get
			{
				return this.authorField;
			}
			set
			{
				this.authorField = value;
				this.RaisePropertyChanged( "Author" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string Binding
		{
			get
			{
				return this.bindingField;
			}
			set
			{
				this.bindingField = value;
				this.RaisePropertyChanged( "Binding" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public string Brand
		{
			get
			{
				return this.brandField;
			}
			set
			{
				this.brandField = value;
				this.RaisePropertyChanged( "Brand" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 8 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "CatalogNumberListElement", IsNullable = false )]
		public string[] CatalogNumberList
		{
			get
			{
				return this.catalogNumberListField;
			}
			set
			{
				this.catalogNumberListField = value;
				this.RaisePropertyChanged( "CatalogNumberList" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Category", Order = 9 )]
		public string[] Category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
				this.RaisePropertyChanged( "Category" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 10 )]
		public string CEROAgeRating
		{
			get
			{
				return this.cEROAgeRatingField;
			}
			set
			{
				this.cEROAgeRatingField = value;
				this.RaisePropertyChanged( "CEROAgeRating" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 11 )]
		public string ClothingSize
		{
			get
			{
				return this.clothingSizeField;
			}
			set
			{
				this.clothingSizeField = value;
				this.RaisePropertyChanged( "ClothingSize" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 12 )]
		public string Color
		{
			get
			{
				return this.colorField;
			}
			set
			{
				this.colorField = value;
				this.RaisePropertyChanged( "Color" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Creator", Order = 13 )]
		public ItemAttributesCreator[] Creator
		{
			get
			{
				return this.creatorField;
			}
			set
			{
				this.creatorField = value;
				this.RaisePropertyChanged( "Creator" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 14 )]
		public string Department
		{
			get
			{
				return this.departmentField;
			}
			set
			{
				this.departmentField = value;
				this.RaisePropertyChanged( "Department" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Director", Order = 15 )]
		public string[] Director
		{
			get
			{
				return this.directorField;
			}
			set
			{
				this.directorField = value;
				this.RaisePropertyChanged( "Director" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 16 )]
		public string EAN
		{
			get
			{
				return this.eANField;
			}
			set
			{
				this.eANField = value;
				this.RaisePropertyChanged( "EAN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 17 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "EANListElement", IsNullable = false )]
		public string[] EANList
		{
			get
			{
				return this.eANListField;
			}
			set
			{
				this.eANListField = value;
				this.RaisePropertyChanged( "EANList" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 18 )]
		public string Edition
		{
			get
			{
				return this.editionField;
			}
			set
			{
				this.editionField = value;
				this.RaisePropertyChanged( "Edition" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "EISBN", Order = 19 )]
		public string[] EISBN
		{
			get
			{
				return this.eISBNField;
			}
			set
			{
				this.eISBNField = value;
				this.RaisePropertyChanged( "EISBN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 20 )]
		public string EpisodeSequence
		{
			get
			{
				return this.episodeSequenceField;
			}
			set
			{
				this.episodeSequenceField = value;
				this.RaisePropertyChanged( "EpisodeSequence" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 21 )]
		public string ESRBAgeRating
		{
			get
			{
				return this.eSRBAgeRatingField;
			}
			set
			{
				this.eSRBAgeRatingField = value;
				this.RaisePropertyChanged( "ESRBAgeRating" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Feature", Order = 22 )]
		public string[] Feature
		{
			get
			{
				return this.featureField;
			}
			set
			{
				this.featureField = value;
				this.RaisePropertyChanged( "Feature" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Format", Order = 23 )]
		public string[] Format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
				this.RaisePropertyChanged( "Format" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 24 )]
		public string Genre
		{
			get
			{
				return this.genreField;
			}
			set
			{
				this.genreField = value;
				this.RaisePropertyChanged( "Genre" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 25 )]
		public string HardwarePlatform
		{
			get
			{
				return this.hardwarePlatformField;
			}
			set
			{
				this.hardwarePlatformField = value;
				this.RaisePropertyChanged( "HardwarePlatform" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 26 )]
		public string HazardousMaterialType
		{
			get
			{
				return this.hazardousMaterialTypeField;
			}
			set
			{
				this.hazardousMaterialTypeField = value;
				this.RaisePropertyChanged( "HazardousMaterialType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 27 )]
		public bool IsAdultProduct
		{
			get
			{
				return this.isAdultProductField;
			}
			set
			{
				this.isAdultProductField = value;
				this.RaisePropertyChanged( "IsAdultProduct" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsAdultProductSpecified
		{
			get
			{
				return this.isAdultProductFieldSpecified;
			}
			set
			{
				this.isAdultProductFieldSpecified = value;
				this.RaisePropertyChanged( "IsAdultProductSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 28 )]
		public bool IsAutographed
		{
			get
			{
				return this.isAutographedField;
			}
			set
			{
				this.isAutographedField = value;
				this.RaisePropertyChanged( "IsAutographed" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsAutographedSpecified
		{
			get
			{
				return this.isAutographedFieldSpecified;
			}
			set
			{
				this.isAutographedFieldSpecified = value;
				this.RaisePropertyChanged( "IsAutographedSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 29 )]
		public string ISBN
		{
			get
			{
				return this.iSBNField;
			}
			set
			{
				this.iSBNField = value;
				this.RaisePropertyChanged( "ISBN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 30 )]
		public bool IsEligibleForTradeIn
		{
			get
			{
				return this.isEligibleForTradeInField;
			}
			set
			{
				this.isEligibleForTradeInField = value;
				this.RaisePropertyChanged( "IsEligibleForTradeIn" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsEligibleForTradeInSpecified
		{
			get
			{
				return this.isEligibleForTradeInFieldSpecified;
			}
			set
			{
				this.isEligibleForTradeInFieldSpecified = value;
				this.RaisePropertyChanged( "IsEligibleForTradeInSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 31 )]
		public bool IsMemorabilia
		{
			get
			{
				return this.isMemorabiliaField;
			}
			set
			{
				this.isMemorabiliaField = value;
				this.RaisePropertyChanged( "IsMemorabilia" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsMemorabiliaSpecified
		{
			get
			{
				return this.isMemorabiliaFieldSpecified;
			}
			set
			{
				this.isMemorabiliaFieldSpecified = value;
				this.RaisePropertyChanged( "IsMemorabiliaSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 32 )]
		public string IssuesPerYear
		{
			get
			{
				return this.issuesPerYearField;
			}
			set
			{
				this.issuesPerYearField = value;
				this.RaisePropertyChanged( "IssuesPerYear" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 33 )]
		public ItemAttributesItemDimensions ItemDimensions
		{
			get
			{
				return this.itemDimensionsField;
			}
			set
			{
				this.itemDimensionsField = value;
				this.RaisePropertyChanged( "ItemDimensions" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 34 )]
		public string ItemPartNumber
		{
			get
			{
				return this.itemPartNumberField;
			}
			set
			{
				this.itemPartNumberField = value;
				this.RaisePropertyChanged( "ItemPartNumber" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 35 )]
		public string Label
		{
			get
			{
				return this.labelField;
			}
			set
			{
				this.labelField = value;
				this.RaisePropertyChanged( "Label" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 36 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Language", IsNullable = false )]
		public ItemAttributesLanguage[] Languages
		{
			get
			{
				return this.languagesField;
			}
			set
			{
				this.languagesField = value;
				this.RaisePropertyChanged( "Languages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 37 )]
		public string LegalDisclaimer
		{
			get
			{
				return this.legalDisclaimerField;
			}
			set
			{
				this.legalDisclaimerField = value;
				this.RaisePropertyChanged( "LegalDisclaimer" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 38 )]
		public Price ListPrice
		{
			get
			{
				return this.listPriceField;
			}
			set
			{
				this.listPriceField = value;
				this.RaisePropertyChanged( "ListPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 39 )]
		public string MagazineType
		{
			get
			{
				return this.magazineTypeField;
			}
			set
			{
				this.magazineTypeField = value;
				this.RaisePropertyChanged( "MagazineType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 40 )]
		public string Manufacturer
		{
			get
			{
				return this.manufacturerField;
			}
			set
			{
				this.manufacturerField = value;
				this.RaisePropertyChanged( "Manufacturer" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 41 )]
		public DecimalWithUnits ManufacturerMaximumAge
		{
			get
			{
				return this.manufacturerMaximumAgeField;
			}
			set
			{
				this.manufacturerMaximumAgeField = value;
				this.RaisePropertyChanged( "ManufacturerMaximumAge" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 42 )]
		public DecimalWithUnits ManufacturerMinimumAge
		{
			get
			{
				return this.manufacturerMinimumAgeField;
			}
			set
			{
				this.manufacturerMinimumAgeField = value;
				this.RaisePropertyChanged( "ManufacturerMinimumAge" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 43 )]
		public string ManufacturerPartsWarrantyDescription
		{
			get
			{
				return this.manufacturerPartsWarrantyDescriptionField;
			}
			set
			{
				this.manufacturerPartsWarrantyDescriptionField = value;
				this.RaisePropertyChanged( "ManufacturerPartsWarrantyDescription" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 44 )]
		public string MediaType
		{
			get
			{
				return this.mediaTypeField;
			}
			set
			{
				this.mediaTypeField = value;
				this.RaisePropertyChanged( "MediaType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 45 )]
		public string Model
		{
			get
			{
				return this.modelField;
			}
			set
			{
				this.modelField = value;
				this.RaisePropertyChanged( "Model" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 46 )]
		public string ModelYear
		{
			get
			{
				return this.modelYearField;
			}
			set
			{
				this.modelYearField = value;
				this.RaisePropertyChanged( "ModelYear" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 47 )]
		public string MPN
		{
			get
			{
				return this.mPNField;
			}
			set
			{
				this.mPNField = value;
				this.RaisePropertyChanged( "MPN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 48 )]
		public string NumberOfDiscs
		{
			get
			{
				return this.numberOfDiscsField;
			}
			set
			{
				this.numberOfDiscsField = value;
				this.RaisePropertyChanged( "NumberOfDiscs" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 49 )]
		public string NumberOfIssues
		{
			get
			{
				return this.numberOfIssuesField;
			}
			set
			{
				this.numberOfIssuesField = value;
				this.RaisePropertyChanged( "NumberOfIssues" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 50 )]
		public string NumberOfItems
		{
			get
			{
				return this.numberOfItemsField;
			}
			set
			{
				this.numberOfItemsField = value;
				this.RaisePropertyChanged( "NumberOfItems" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 51 )]
		public string NumberOfPages
		{
			get
			{
				return this.numberOfPagesField;
			}
			set
			{
				this.numberOfPagesField = value;
				this.RaisePropertyChanged( "NumberOfPages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 52 )]
		public string NumberOfTracks
		{
			get
			{
				return this.numberOfTracksField;
			}
			set
			{
				this.numberOfTracksField = value;
				this.RaisePropertyChanged( "NumberOfTracks" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 53 )]
		public string OperatingSystem
		{
			get
			{
				return this.operatingSystemField;
			}
			set
			{
				this.operatingSystemField = value;
				this.RaisePropertyChanged( "OperatingSystem" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 54 )]
		public ItemAttributesPackageDimensions PackageDimensions
		{
			get
			{
				return this.packageDimensionsField;
			}
			set
			{
				this.packageDimensionsField = value;
				this.RaisePropertyChanged( "PackageDimensions" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 55 )]
		public string PackageQuantity
		{
			get
			{
				return this.packageQuantityField;
			}
			set
			{
				this.packageQuantityField = value;
				this.RaisePropertyChanged( "PackageQuantity" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 56 )]
		public string PartNumber
		{
			get
			{
				return this.partNumberField;
			}
			set
			{
				this.partNumberField = value;
				this.RaisePropertyChanged( "PartNumber" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "PictureFormat", Order = 57 )]
		public string[] PictureFormat
		{
			get
			{
				return this.pictureFormatField;
			}
			set
			{
				this.pictureFormatField = value;
				this.RaisePropertyChanged( "PictureFormat" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Platform", Order = 58 )]
		public string[] Platform
		{
			get
			{
				return this.platformField;
			}
			set
			{
				this.platformField = value;
				this.RaisePropertyChanged( "Platform" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 59 )]
		public string ProductGroup
		{
			get
			{
				return this.productGroupField;
			}
			set
			{
				this.productGroupField = value;
				this.RaisePropertyChanged( "ProductGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 60 )]
		public string ProductTypeName
		{
			get
			{
				return this.productTypeNameField;
			}
			set
			{
				this.productTypeNameField = value;
				this.RaisePropertyChanged( "ProductTypeName" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 61 )]
		public string ProductTypeSubcategory
		{
			get
			{
				return this.productTypeSubcategoryField;
			}
			set
			{
				this.productTypeSubcategoryField = value;
				this.RaisePropertyChanged( "ProductTypeSubcategory" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 62 )]
		public string PublicationDate
		{
			get
			{
				return this.publicationDateField;
			}
			set
			{
				this.publicationDateField = value;
				this.RaisePropertyChanged( "PublicationDate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 63 )]
		public string Publisher
		{
			get
			{
				return this.publisherField;
			}
			set
			{
				this.publisherField = value;
				this.RaisePropertyChanged( "Publisher" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 64 )]
		public string RegionCode
		{
			get
			{
				return this.regionCodeField;
			}
			set
			{
				this.regionCodeField = value;
				this.RaisePropertyChanged( "RegionCode" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 65 )]
		public string ReleaseDate
		{
			get
			{
				return this.releaseDateField;
			}
			set
			{
				this.releaseDateField = value;
				this.RaisePropertyChanged( "ReleaseDate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 66 )]
		public DecimalWithUnits RunningTime
		{
			get
			{
				return this.runningTimeField;
			}
			set
			{
				this.runningTimeField = value;
				this.RaisePropertyChanged( "RunningTime" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 67 )]
		public string SeikodoProductCode
		{
			get
			{
				return this.seikodoProductCodeField;
			}
			set
			{
				this.seikodoProductCodeField = value;
				this.RaisePropertyChanged( "SeikodoProductCode" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 68 )]
		public string Size
		{
			get
			{
				return this.sizeField;
			}
			set
			{
				this.sizeField = value;
				this.RaisePropertyChanged( "Size" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 69 )]
		public string SKU
		{
			get
			{
				return this.sKUField;
			}
			set
			{
				this.sKUField = value;
				this.RaisePropertyChanged( "SKU" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 70 )]
		public string Studio
		{
			get
			{
				return this.studioField;
			}
			set
			{
				this.studioField = value;
				this.RaisePropertyChanged( "Studio" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 71 )]
		public NonNegativeIntegerWithUnits SubscriptionLength
		{
			get
			{
				return this.subscriptionLengthField;
			}
			set
			{
				this.subscriptionLengthField = value;
				this.RaisePropertyChanged( "SubscriptionLength" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 72 )]
		public string Title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
				this.RaisePropertyChanged( "Title" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 73 )]
		public string TrackSequence
		{
			get
			{
				return this.trackSequenceField;
			}
			set
			{
				this.trackSequenceField = value;
				this.RaisePropertyChanged( "TrackSequence" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 74 )]
		public Price TradeInValue
		{
			get
			{
				return this.tradeInValueField;
			}
			set
			{
				this.tradeInValueField = value;
				this.RaisePropertyChanged( "TradeInValue" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 75 )]
		public string UPC
		{
			get
			{
				return this.uPCField;
			}
			set
			{
				this.uPCField = value;
				this.RaisePropertyChanged( "UPC" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 76 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "UPCListElement", IsNullable = false )]
		public string[] UPCList
		{
			get
			{
				return this.uPCListField;
			}
			set
			{
				this.uPCListField = value;
				this.RaisePropertyChanged( "UPCList" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 77 )]
		public string Warranty
		{
			get
			{
				return this.warrantyField;
			}
			set
			{
				this.warrantyField = value;
				this.RaisePropertyChanged( "Warranty" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 78 )]
		public Price WEEETaxValue
		{
			get
			{
				return this.wEEETaxValueField;
			}
			set
			{
				this.wEEETaxValueField = value;
				this.RaisePropertyChanged( "WEEETaxValue" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}