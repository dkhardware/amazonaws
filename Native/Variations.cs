namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Variations : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string totalVariationsField;

		private string totalVariationPagesField;

		private string[] variationDimensionsField;

		private Item[] itemField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 0 )]
		public string TotalVariations
		{
			get
			{
				return this.totalVariationsField;
			}
			set
			{
				this.totalVariationsField = value;
				this.RaisePropertyChanged( "TotalVariations" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 1 )]
		public string TotalVariationPages
		{
			get
			{
				return this.totalVariationPagesField;
			}
			set
			{
				this.totalVariationPagesField = value;
				this.RaisePropertyChanged( "TotalVariationPages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 2 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "VariationDimension", IsNullable = false )]
		public string[] VariationDimensions
		{
			get
			{
				return this.variationDimensionsField;
			}
			set
			{
				this.variationDimensionsField = value;
				this.RaisePropertyChanged( "VariationDimensions" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Item", Order = 3 )]
		public Item[] Item
		{
			get
			{
				return this.itemField;
			}
			set
			{
				this.itemField = value;
				this.RaisePropertyChanged( "Item" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}