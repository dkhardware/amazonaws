namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartAddRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string cartIdField;

		private string hMACField;

		private string mergeCartField;

		private CartAddRequestItem[] itemsField;

		private string[] responseGroupField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string CartId
		{
			get
			{
				return this.cartIdField;
			}
			set
			{
				this.cartIdField = value;
				this.RaisePropertyChanged( "CartId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string HMAC
		{
			get
			{
				return this.hMACField;
			}
			set
			{
				this.hMACField = value;
				this.RaisePropertyChanged( "HMAC" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string MergeCart
		{
			get
			{
				return this.mergeCartField;
			}
			set
			{
				this.mergeCartField = value;
				this.RaisePropertyChanged( "MergeCart" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 3 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Item", IsNullable = false )]
		public CartAddRequestItem[] Items
		{
			get
			{
				return this.itemsField;
			}
			set
			{
				this.itemsField = value;
				this.RaisePropertyChanged( "Items" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 4 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}