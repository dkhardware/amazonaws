namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class SavedForLaterItems : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Price subTotalField;

		private CartItem[] savedForLaterItemField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Price SubTotal
		{
			get
			{
				return this.subTotalField;
			}
			set
			{
				this.subTotalField = value;
				this.RaisePropertyChanged( "SubTotal" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "SavedForLaterItem", Order = 1 )]
		public CartItem[] SavedForLaterItem
		{
			get
			{
				return this.savedForLaterItemField;
			}
			set
			{
				this.savedForLaterItemField = value;
				this.RaisePropertyChanged( "SavedForLaterItem" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}