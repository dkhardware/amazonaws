namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public enum Condition
	{

		/// <remarks/>
		All,

		/// <remarks/>
		New,

		/// <remarks/>
		Used,

		/// <remarks/>
		Collectible,

		/// <remarks/>
		Refurbished,
	}
}