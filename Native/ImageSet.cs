namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class ImageSet : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Image swatchImageField;

		private Image smallImageField;

		private Image thumbnailImageField;

		private Image tinyImageField;

		private Image mediumImageField;

		private Image largeImageField;

		private string categoryField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Image SwatchImage
		{
			get
			{
				return this.swatchImageField;
			}
			set
			{
				this.swatchImageField = value;
				this.RaisePropertyChanged( "SwatchImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Image SmallImage
		{
			get
			{
				return this.smallImageField;
			}
			set
			{
				this.smallImageField = value;
				this.RaisePropertyChanged( "SmallImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public Image ThumbnailImage
		{
			get
			{
				return this.thumbnailImageField;
			}
			set
			{
				this.thumbnailImageField = value;
				this.RaisePropertyChanged( "ThumbnailImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public Image TinyImage
		{
			get
			{
				return this.tinyImageField;
			}
			set
			{
				this.tinyImageField = value;
				this.RaisePropertyChanged( "TinyImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public Image MediumImage
		{
			get
			{
				return this.mediumImageField;
			}
			set
			{
				this.mediumImageField = value;
				this.RaisePropertyChanged( "MediumImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public Image LargeImage
		{
			get
			{
				return this.largeImageField;
			}
			set
			{
				this.largeImageField = value;
				this.RaisePropertyChanged( "LargeImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string Category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
				this.RaisePropertyChanged( "Category" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}