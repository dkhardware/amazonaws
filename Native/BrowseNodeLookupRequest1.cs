namespace Amazon.AWS.Native
{
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
	[System.ServiceModel.MessageContractAttribute( IsWrapped = false )]
	public class BrowseNodeLookupRequest1
	{

		[System.ServiceModel.MessageBodyMemberAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01", Order = 0 )]
		public BrowseNodeLookup BrowseNodeLookup;

		public BrowseNodeLookupRequest1()
		{
		}

		public BrowseNodeLookupRequest1( BrowseNodeLookup BrowseNodeLookup )
		{
			this.BrowseNodeLookup = BrowseNodeLookup;
		}
	}
}