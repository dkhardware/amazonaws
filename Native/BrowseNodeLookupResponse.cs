namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class BrowseNodeLookupResponse : object, System.ComponentModel.INotifyPropertyChanged
	{

		private OperationRequest operationRequestField;

		private BrowseNodes[] browseNodesField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public OperationRequest OperationRequest
		{
			get
			{
				return this.operationRequestField;
			}
			set
			{
				this.operationRequestField = value;
				this.RaisePropertyChanged( "OperationRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "BrowseNodes", Order = 1 )]
		public BrowseNodes[] BrowseNodes
		{
			get
			{
				return this.browseNodesField;
			}
			set
			{
				this.browseNodesField = value;
				this.RaisePropertyChanged( "BrowseNodes" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}