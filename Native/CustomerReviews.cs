namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CustomerReviews : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string iFrameURLField;

		private bool hasReviewsField;

		private bool hasReviewsFieldSpecified;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string IFrameURL
		{
			get
			{
				return this.iFrameURLField;
			}
			set
			{
				this.iFrameURLField = value;
				this.RaisePropertyChanged( "IFrameURL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public bool HasReviews
		{
			get
			{
				return this.hasReviewsField;
			}
			set
			{
				this.hasReviewsField = value;
				this.RaisePropertyChanged( "HasReviews" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool HasReviewsSpecified
		{
			get
			{
				return this.hasReviewsFieldSpecified;
			}
			set
			{
				this.hasReviewsFieldSpecified = value;
				this.RaisePropertyChanged( "HasReviewsSpecified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}