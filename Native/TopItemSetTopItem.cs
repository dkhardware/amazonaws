namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class TopItemSetTopItem : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string aSINField;

		private string titleField;

		private string detailPageURLField;

		private string productGroupField;

		private string[] authorField;

		private string[] artistField;

		private string[] actorField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string ASIN
		{
			get
			{
				return this.aSINField;
			}
			set
			{
				this.aSINField = value;
				this.RaisePropertyChanged( "ASIN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string Title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
				this.RaisePropertyChanged( "Title" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string DetailPageURL
		{
			get
			{
				return this.detailPageURLField;
			}
			set
			{
				this.detailPageURLField = value;
				this.RaisePropertyChanged( "DetailPageURL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string ProductGroup
		{
			get
			{
				return this.productGroupField;
			}
			set
			{
				this.productGroupField = value;
				this.RaisePropertyChanged( "ProductGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Author", Order = 4 )]
		public string[] Author
		{
			get
			{
				return this.authorField;
			}
			set
			{
				this.authorField = value;
				this.RaisePropertyChanged( "Author" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Artist", Order = 5 )]
		public string[] Artist
		{
			get
			{
				return this.artistField;
			}
			set
			{
				this.artistField = value;
				this.RaisePropertyChanged( "Artist" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Actor", Order = 6 )]
		public string[] Actor
		{
			get
			{
				return this.actorField;
			}
			set
			{
				this.actorField = value;
				this.RaisePropertyChanged( "Actor" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}