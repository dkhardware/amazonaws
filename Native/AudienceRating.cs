namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public enum AudienceRating
	{

		/// <remarks/>
		G,

		/// <remarks/>
		PG,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "PG-13" )]
		PG13,

		/// <remarks/>
		R,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "NC-17" )]
		NC17,

		/// <remarks/>
		NR,

		/// <remarks/>
		Unrated,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "6" )]
		Item6,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "12" )]
		Item12,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "16" )]
		Item16,

		/// <remarks/>
		[System.Xml.Serialization.XmlEnumAttribute( "18" )]
		Item18,

		/// <remarks/>
		FamilyViewing,
	}
}