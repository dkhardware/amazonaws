namespace Amazon.AWS.Native
{
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
	[System.ServiceModel.MessageContractAttribute( IsWrapped = false )]
	public class SimilarityLookupRequest1
	{

		[System.ServiceModel.MessageBodyMemberAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01", Order = 0 )]
		public SimilarityLookup SimilarityLookup;

		public SimilarityLookupRequest1()
		{
		}

		public SimilarityLookupRequest1( SimilarityLookup SimilarityLookup )
		{
			this.SimilarityLookup = SimilarityLookup;
		}
	}
}