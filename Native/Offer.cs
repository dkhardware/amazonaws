namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Offer : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Merchant merchantField;

		private OfferAttributes offerAttributesField;

		private OfferListing[] offerListingField;

		private LoyaltyPoints loyaltyPointsField;

		private Promotion[] promotionsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Merchant Merchant
		{
			get
			{
				return this.merchantField;
			}
			set
			{
				this.merchantField = value;
				this.RaisePropertyChanged( "Merchant" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public OfferAttributes OfferAttributes
		{
			get
			{
				return this.offerAttributesField;
			}
			set
			{
				this.offerAttributesField = value;
				this.RaisePropertyChanged( "OfferAttributes" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "OfferListing", Order = 2 )]
		public OfferListing[] OfferListing
		{
			get
			{
				return this.offerListingField;
			}
			set
			{
				this.offerListingField = value;
				this.RaisePropertyChanged( "OfferListing" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public LoyaltyPoints LoyaltyPoints
		{
			get
			{
				return this.loyaltyPointsField;
			}
			set
			{
				this.loyaltyPointsField = value;
				this.RaisePropertyChanged( "LoyaltyPoints" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 4 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Promotion", IsNullable = false )]
		public Promotion[] Promotions
		{
			get
			{
				return this.promotionsField;
			}
			set
			{
				this.promotionsField = value;
				this.RaisePropertyChanged( "Promotions" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}