namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CollectionsCollectionCollectionSummary : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Price lowestListPriceField;

		private Price highestListPriceField;

		private Price lowestSalePriceField;

		private Price highestSalePriceField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Price LowestListPrice
		{
			get
			{
				return this.lowestListPriceField;
			}
			set
			{
				this.lowestListPriceField = value;
				this.RaisePropertyChanged( "LowestListPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Price HighestListPrice
		{
			get
			{
				return this.highestListPriceField;
			}
			set
			{
				this.highestListPriceField = value;
				this.RaisePropertyChanged( "HighestListPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public Price LowestSalePrice
		{
			get
			{
				return this.lowestSalePriceField;
			}
			set
			{
				this.lowestSalePriceField = value;
				this.RaisePropertyChanged( "LowestSalePrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public Price HighestSalePrice
		{
			get
			{
				return this.highestSalePriceField;
			}
			set
			{
				this.highestSalePriceField = value;
				this.RaisePropertyChanged( "HighestSalePrice" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}