namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class OfferSummary : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Price lowestNewPriceField;

		private Price lowestUsedPriceField;

		private Price lowestCollectiblePriceField;

		private Price lowestRefurbishedPriceField;

		private string totalNewField;

		private string totalUsedField;

		private string totalCollectibleField;

		private string totalRefurbishedField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Price LowestNewPrice
		{
			get
			{
				return this.lowestNewPriceField;
			}
			set
			{
				this.lowestNewPriceField = value;
				this.RaisePropertyChanged( "LowestNewPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Price LowestUsedPrice
		{
			get
			{
				return this.lowestUsedPriceField;
			}
			set
			{
				this.lowestUsedPriceField = value;
				this.RaisePropertyChanged( "LowestUsedPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public Price LowestCollectiblePrice
		{
			get
			{
				return this.lowestCollectiblePriceField;
			}
			set
			{
				this.lowestCollectiblePriceField = value;
				this.RaisePropertyChanged( "LowestCollectiblePrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public Price LowestRefurbishedPrice
		{
			get
			{
				return this.lowestRefurbishedPriceField;
			}
			set
			{
				this.lowestRefurbishedPriceField = value;
				this.RaisePropertyChanged( "LowestRefurbishedPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string TotalNew
		{
			get
			{
				return this.totalNewField;
			}
			set
			{
				this.totalNewField = value;
				this.RaisePropertyChanged( "TotalNew" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string TotalUsed
		{
			get
			{
				return this.totalUsedField;
			}
			set
			{
				this.totalUsedField = value;
				this.RaisePropertyChanged( "TotalUsed" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string TotalCollectible
		{
			get
			{
				return this.totalCollectibleField;
			}
			set
			{
				this.totalCollectibleField = value;
				this.RaisePropertyChanged( "TotalCollectible" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public string TotalRefurbished
		{
			get
			{
				return this.totalRefurbishedField;
			}
			set
			{
				this.totalRefurbishedField = value;
				this.RaisePropertyChanged( "TotalRefurbished" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}