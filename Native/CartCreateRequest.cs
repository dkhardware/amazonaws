namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartCreateRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string mergeCartField;

		private CartCreateRequestItem[] itemsField;

		private string[] responseGroupField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string MergeCart
		{
			get
			{
				return this.mergeCartField;
			}
			set
			{
				this.mergeCartField = value;
				this.RaisePropertyChanged( "MergeCart" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 1 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Item", IsNullable = false )]
		public CartCreateRequestItem[] Items
		{
			get
			{
				return this.itemsField;
			}
			set
			{
				this.itemsField = value;
				this.RaisePropertyChanged( "Items" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 2 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}