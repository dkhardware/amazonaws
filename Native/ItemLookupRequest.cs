namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class ItemLookupRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Condition conditionField;

		private bool conditionFieldSpecified;

		private ItemLookupRequestIdType idTypeField;

		private bool idTypeFieldSpecified;

		private string merchantIdField;

		private string[] itemIdField;

		private string[] responseGroupField;

		private string searchIndexField;

		private string variationPageField;

		private string relatedItemPageField;

		private string[] relationshipTypeField;

		private string includeReviewsSummaryField;

		private string truncateReviewsAtField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Condition Condition
		{
			get
			{
				return this.conditionField;
			}
			set
			{
				this.conditionField = value;
				this.RaisePropertyChanged( "Condition" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ConditionSpecified
		{
			get
			{
				return this.conditionFieldSpecified;
			}
			set
			{
				this.conditionFieldSpecified = value;
				this.RaisePropertyChanged( "ConditionSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public ItemLookupRequestIdType IdType
		{
			get
			{
				return this.idTypeField;
			}
			set
			{
				this.idTypeField = value;
				this.RaisePropertyChanged( "IdType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IdTypeSpecified
		{
			get
			{
				return this.idTypeFieldSpecified;
			}
			set
			{
				this.idTypeFieldSpecified = value;
				this.RaisePropertyChanged( "IdTypeSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string MerchantId
		{
			get
			{
				return this.merchantIdField;
			}
			set
			{
				this.merchantIdField = value;
				this.RaisePropertyChanged( "MerchantId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ItemId", Order = 3 )]
		public string[] ItemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
				this.RaisePropertyChanged( "ItemId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 4 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string SearchIndex
		{
			get
			{
				return this.searchIndexField;
			}
			set
			{
				this.searchIndexField = value;
				this.RaisePropertyChanged( "SearchIndex" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string VariationPage
		{
			get
			{
				return this.variationPageField;
			}
			set
			{
				this.variationPageField = value;
				this.RaisePropertyChanged( "VariationPage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public string RelatedItemPage
		{
			get
			{
				return this.relatedItemPageField;
			}
			set
			{
				this.relatedItemPageField = value;
				this.RaisePropertyChanged( "RelatedItemPage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "RelationshipType", Order = 8 )]
		public string[] RelationshipType
		{
			get
			{
				return this.relationshipTypeField;
			}
			set
			{
				this.relationshipTypeField = value;
				this.RaisePropertyChanged( "RelationshipType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 9 )]
		public string IncludeReviewsSummary
		{
			get
			{
				return this.includeReviewsSummaryField;
			}
			set
			{
				this.includeReviewsSummaryField = value;
				this.RaisePropertyChanged( "IncludeReviewsSummary" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 10 )]
		public string TruncateReviewsAt
		{
			get
			{
				return this.truncateReviewsAtField;
			}
			set
			{
				this.truncateReviewsAtField = value;
				this.RaisePropertyChanged( "TruncateReviewsAt" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}