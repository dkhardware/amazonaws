namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class OperationRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private HTTPHeadersHeader[] hTTPHeadersField;

		private string requestIdField;

		private ArgumentsArgument[] argumentsField;

		private ErrorsError[] errorsField;

		private float requestProcessingTimeField;

		private bool requestProcessingTimeFieldSpecified;

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 0 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Header", IsNullable = false )]
		public HTTPHeadersHeader[] HTTPHeaders
		{
			get
			{
				return this.hTTPHeadersField;
			}
			set
			{
				this.hTTPHeadersField = value;
				this.RaisePropertyChanged( "HTTPHeaders" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string RequestId
		{
			get
			{
				return this.requestIdField;
			}
			set
			{
				this.requestIdField = value;
				this.RaisePropertyChanged( "RequestId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 2 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Argument", IsNullable = false )]
		public ArgumentsArgument[] Arguments
		{
			get
			{
				return this.argumentsField;
			}
			set
			{
				this.argumentsField = value;
				this.RaisePropertyChanged( "Arguments" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 3 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Error", IsNullable = false )]
		public ErrorsError[] Errors
		{
			get
			{
				return this.errorsField;
			}
			set
			{
				this.errorsField = value;
				this.RaisePropertyChanged( "Errors" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public float RequestProcessingTime
		{
			get
			{
				return this.requestProcessingTimeField;
			}
			set
			{
				this.requestProcessingTimeField = value;
				this.RaisePropertyChanged( "RequestProcessingTime" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool RequestProcessingTimeSpecified
		{
			get
			{
				return this.requestProcessingTimeFieldSpecified;
			}
			set
			{
				this.requestProcessingTimeFieldSpecified = value;
				this.RaisePropertyChanged( "RequestProcessingTimeSpecified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}