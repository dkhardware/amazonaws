namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Price : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string amountField;

		private string currencyCodeField;

		private string formattedPriceField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "integer", Order = 0 )]
		public string Amount
		{
			get
			{
				return this.amountField;
			}
			set
			{
				this.amountField = value;
				this.RaisePropertyChanged( "Amount" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string CurrencyCode
		{
			get
			{
				return this.currencyCodeField;
			}
			set
			{
				this.currencyCodeField = value;
				this.RaisePropertyChanged( "CurrencyCode" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string FormattedPrice
		{
			get
			{
				return this.formattedPriceField;
			}
			set
			{
				this.formattedPriceField = value;
				this.RaisePropertyChanged( "FormattedPrice" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}