namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class PromotionSummary : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string promotionIdField;

		private string categoryField;

		private string startDateField;

		private string endDateField;

		private string eligibilityRequirementDescriptionField;

		private string benefitDescriptionField;

		private string termsAndConditionsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string PromotionId
		{
			get
			{
				return this.promotionIdField;
			}
			set
			{
				this.promotionIdField = value;
				this.RaisePropertyChanged( "PromotionId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string Category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
				this.RaisePropertyChanged( "Category" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string StartDate
		{
			get
			{
				return this.startDateField;
			}
			set
			{
				this.startDateField = value;
				this.RaisePropertyChanged( "StartDate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string EndDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
				this.RaisePropertyChanged( "EndDate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string EligibilityRequirementDescription
		{
			get
			{
				return this.eligibilityRequirementDescriptionField;
			}
			set
			{
				this.eligibilityRequirementDescriptionField = value;
				this.RaisePropertyChanged( "EligibilityRequirementDescription" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string BenefitDescription
		{
			get
			{
				return this.benefitDescriptionField;
			}
			set
			{
				this.benefitDescriptionField = value;
				this.RaisePropertyChanged( "BenefitDescription" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string TermsAndConditions
		{
			get
			{
				return this.termsAndConditionsField;
			}
			set
			{
				this.termsAndConditionsField = value;
				this.RaisePropertyChanged( "TermsAndConditions" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}