namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class ItemSearchRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string actorField;

		private string artistField;

		private ItemSearchRequestAvailability availabilityField;

		private bool availabilityFieldSpecified;

		private AudienceRating[] audienceRatingField;

		private string authorField;

		private string brandField;

		private string browseNodeField;

		private string composerField;

		private Condition conditionField;

		private bool conditionFieldSpecified;

		private string conductorField;

		private string directorField;

		private string itemPageField;

		private string keywordsField;

		private string manufacturerField;

		private string maximumPriceField;

		private string merchantIdField;

		private string minimumPriceField;

		private string minPercentageOffField;

		private string musicLabelField;

		private string orchestraField;

		private string powerField;

		private string publisherField;

		private string relatedItemPageField;

		private string[] relationshipTypeField;

		private string[] responseGroupField;

		private string searchIndexField;

		private string sortField;

		private string titleField;

		private string releaseDateField;

		private string includeReviewsSummaryField;

		private string truncateReviewsAtField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string Actor
		{
			get
			{
				return this.actorField;
			}
			set
			{
				this.actorField = value;
				this.RaisePropertyChanged( "Actor" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string Artist
		{
			get
			{
				return this.artistField;
			}
			set
			{
				this.artistField = value;
				this.RaisePropertyChanged( "Artist" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public ItemSearchRequestAvailability Availability
		{
			get
			{
				return this.availabilityField;
			}
			set
			{
				this.availabilityField = value;
				this.RaisePropertyChanged( "Availability" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool AvailabilitySpecified
		{
			get
			{
				return this.availabilityFieldSpecified;
			}
			set
			{
				this.availabilityFieldSpecified = value;
				this.RaisePropertyChanged( "AvailabilitySpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "AudienceRating", Order = 3 )]
		public AudienceRating[] AudienceRating
		{
			get
			{
				return this.audienceRatingField;
			}
			set
			{
				this.audienceRatingField = value;
				this.RaisePropertyChanged( "AudienceRating" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string Author
		{
			get
			{
				return this.authorField;
			}
			set
			{
				this.authorField = value;
				this.RaisePropertyChanged( "Author" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string Brand
		{
			get
			{
				return this.brandField;
			}
			set
			{
				this.brandField = value;
				this.RaisePropertyChanged( "Brand" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string BrowseNode
		{
			get
			{
				return this.browseNodeField;
			}
			set
			{
				this.browseNodeField = value;
				this.RaisePropertyChanged( "BrowseNode" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public string Composer
		{
			get
			{
				return this.composerField;
			}
			set
			{
				this.composerField = value;
				this.RaisePropertyChanged( "Composer" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public Condition Condition
		{
			get
			{
				return this.conditionField;
			}
			set
			{
				this.conditionField = value;
				this.RaisePropertyChanged( "Condition" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ConditionSpecified
		{
			get
			{
				return this.conditionFieldSpecified;
			}
			set
			{
				this.conditionFieldSpecified = value;
				this.RaisePropertyChanged( "ConditionSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 9 )]
		public string Conductor
		{
			get
			{
				return this.conductorField;
			}
			set
			{
				this.conductorField = value;
				this.RaisePropertyChanged( "Conductor" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 10 )]
		public string Director
		{
			get
			{
				return this.directorField;
			}
			set
			{
				this.directorField = value;
				this.RaisePropertyChanged( "Director" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "positiveInteger", Order = 11 )]
		public string ItemPage
		{
			get
			{
				return this.itemPageField;
			}
			set
			{
				this.itemPageField = value;
				this.RaisePropertyChanged( "ItemPage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 12 )]
		public string Keywords
		{
			get
			{
				return this.keywordsField;
			}
			set
			{
				this.keywordsField = value;
				this.RaisePropertyChanged( "Keywords" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 13 )]
		public string Manufacturer
		{
			get
			{
				return this.manufacturerField;
			}
			set
			{
				this.manufacturerField = value;
				this.RaisePropertyChanged( "Manufacturer" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 14 )]
		public string MaximumPrice
		{
			get
			{
				return this.maximumPriceField;
			}
			set
			{
				this.maximumPriceField = value;
				this.RaisePropertyChanged( "MaximumPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 15 )]
		public string MerchantId
		{
			get
			{
				return this.merchantIdField;
			}
			set
			{
				this.merchantIdField = value;
				this.RaisePropertyChanged( "MerchantId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 16 )]
		public string MinimumPrice
		{
			get
			{
				return this.minimumPriceField;
			}
			set
			{
				this.minimumPriceField = value;
				this.RaisePropertyChanged( "MinimumPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 17 )]
		public string MinPercentageOff
		{
			get
			{
				return this.minPercentageOffField;
			}
			set
			{
				this.minPercentageOffField = value;
				this.RaisePropertyChanged( "MinPercentageOff" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 18 )]
		public string MusicLabel
		{
			get
			{
				return this.musicLabelField;
			}
			set
			{
				this.musicLabelField = value;
				this.RaisePropertyChanged( "MusicLabel" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 19 )]
		public string Orchestra
		{
			get
			{
				return this.orchestraField;
			}
			set
			{
				this.orchestraField = value;
				this.RaisePropertyChanged( "Orchestra" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 20 )]
		public string Power
		{
			get
			{
				return this.powerField;
			}
			set
			{
				this.powerField = value;
				this.RaisePropertyChanged( "Power" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 21 )]
		public string Publisher
		{
			get
			{
				return this.publisherField;
			}
			set
			{
				this.publisherField = value;
				this.RaisePropertyChanged( "Publisher" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 22 )]
		public string RelatedItemPage
		{
			get
			{
				return this.relatedItemPageField;
			}
			set
			{
				this.relatedItemPageField = value;
				this.RaisePropertyChanged( "RelatedItemPage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "RelationshipType", Order = 23 )]
		public string[] RelationshipType
		{
			get
			{
				return this.relationshipTypeField;
			}
			set
			{
				this.relationshipTypeField = value;
				this.RaisePropertyChanged( "RelationshipType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 24 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 25 )]
		public string SearchIndex
		{
			get
			{
				return this.searchIndexField;
			}
			set
			{
				this.searchIndexField = value;
				this.RaisePropertyChanged( "SearchIndex" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 26 )]
		public string Sort
		{
			get
			{
				return this.sortField;
			}
			set
			{
				this.sortField = value;
				this.RaisePropertyChanged( "Sort" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 27 )]
		public string Title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
				this.RaisePropertyChanged( "Title" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 28 )]
		public string ReleaseDate
		{
			get
			{
				return this.releaseDateField;
			}
			set
			{
				this.releaseDateField = value;
				this.RaisePropertyChanged( "ReleaseDate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 29 )]
		public string IncludeReviewsSummary
		{
			get
			{
				return this.includeReviewsSummaryField;
			}
			set
			{
				this.includeReviewsSummaryField = value;
				this.RaisePropertyChanged( "IncludeReviewsSummary" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 30 )]
		public string TruncateReviewsAt
		{
			get
			{
				return this.truncateReviewsAtField;
			}
			set
			{
				this.truncateReviewsAtField = value;
				this.RaisePropertyChanged( "TruncateReviewsAt" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}