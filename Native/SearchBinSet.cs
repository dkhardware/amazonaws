namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class SearchBinSet : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Bin[] binField;

		private string narrowByField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Bin", Order = 0 )]
		public Bin[] Bin
		{
			get
			{
				return this.binField;
			}
			set
			{
				this.binField = value;
				this.RaisePropertyChanged( "Bin" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string NarrowBy
		{
			get
			{
				return this.narrowByField;
			}
			set
			{
				this.narrowByField = value;
				this.RaisePropertyChanged( "NarrowBy" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}