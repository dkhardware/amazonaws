namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartModifyRequestItem : object, System.ComponentModel.INotifyPropertyChanged
	{

		private CartModifyRequestItemAction actionField;

		private bool actionFieldSpecified;

		private string cartItemIdField;

		private string quantityField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public CartModifyRequestItemAction Action
		{
			get
			{
				return this.actionField;
			}
			set
			{
				this.actionField = value;
				this.RaisePropertyChanged( "Action" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ActionSpecified
		{
			get
			{
				return this.actionFieldSpecified;
			}
			set
			{
				this.actionFieldSpecified = value;
				this.RaisePropertyChanged( "ActionSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string CartItemId
		{
			get
			{
				return this.cartItemIdField;
			}
			set
			{
				this.cartItemIdField = value;
				this.RaisePropertyChanged( "CartItemId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 2 )]
		public string Quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
				this.RaisePropertyChanged( "Quantity" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}