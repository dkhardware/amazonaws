namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class EditorialReview : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string sourceField;

		private string contentField;

		private bool isLinkSuppressedField;

		private bool isLinkSuppressedFieldSpecified;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string Source
		{
			get
			{
				return this.sourceField;
			}
			set
			{
				this.sourceField = value;
				this.RaisePropertyChanged( "Source" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string Content
		{
			get
			{
				return this.contentField;
			}
			set
			{
				this.contentField = value;
				this.RaisePropertyChanged( "Content" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public bool IsLinkSuppressed
		{
			get
			{
				return this.isLinkSuppressedField;
			}
			set
			{
				this.isLinkSuppressedField = value;
				this.RaisePropertyChanged( "IsLinkSuppressed" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsLinkSuppressedSpecified
		{
			get
			{
				return this.isLinkSuppressedFieldSpecified;
			}
			set
			{
				this.isLinkSuppressedFieldSpecified = value;
				this.RaisePropertyChanged( "IsLinkSuppressedSpecified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}