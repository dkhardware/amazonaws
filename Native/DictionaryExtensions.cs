using System;
using System.Collections.Generic;
using System.Text;

namespace Amazon.AWS.Native
{
	public static class DictionaryExtensions
	{
		public static string ToString<TKey, TValue>(
			this IDictionary<TKey, TValue> dictionary, string keyValueSeparator, string sequenceSeparator )
		{
			var stringBuilder = new StringBuilder();
			dictionary.ForEach(
				x => stringBuilder.AppendFormat( "{0}{1}{2}{3}", x.Key.ToString(), keyValueSeparator, x.Value.ToString(), sequenceSeparator ) );

			return stringBuilder.ToString( 0, stringBuilder.Length - sequenceSeparator.Length );
		}

		public static void ForEach<T>( this IEnumerable<T> items, Action<T> action )
		{
			foreach ( var item in items )
			{
				action( item );
			}
		}
	}
}