namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartCreateRequestItem : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string aSINField;

		private string offerListingIdField;

		private string quantityField;

		private string associateTagField;

		private string listItemIdField;

		private CartCreateRequestItemMetaData[] metaDataField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string ASIN
		{
			get
			{
				return this.aSINField;
			}
			set
			{
				this.aSINField = value;
				this.RaisePropertyChanged( "ASIN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string OfferListingId
		{
			get
			{
				return this.offerListingIdField;
			}
			set
			{
				this.offerListingIdField = value;
				this.RaisePropertyChanged( "OfferListingId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "positiveInteger", Order = 2 )]
		public string Quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
				this.RaisePropertyChanged( "Quantity" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string AssociateTag
		{
			get
			{
				return this.associateTagField;
			}
			set
			{
				this.associateTagField = value;
				this.RaisePropertyChanged( "AssociateTag" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string ListItemId
		{
			get
			{
				return this.listItemIdField;
			}
			set
			{
				this.listItemIdField = value;
				this.RaisePropertyChanged( "ListItemId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "MetaData", Order = 5 )]
		public CartCreateRequestItemMetaData[] MetaData
		{
			get
			{
				return this.metaDataField;
			}
			set
			{
				this.metaDataField = value;
				this.RaisePropertyChanged( "MetaData" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}