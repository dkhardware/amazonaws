namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class SearchResultsMapSearchIndex : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string indexNameField;

		private string resultsField;

		private string pagesField;

		private CorrectedQuery correctedQueryField;

		private string relevanceRankField;

		private string[] aSINField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string IndexName
		{
			get
			{
				return this.indexNameField;
			}
			set
			{
				this.indexNameField = value;
				this.RaisePropertyChanged( "IndexName" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 1 )]
		public string Results
		{
			get
			{
				return this.resultsField;
			}
			set
			{
				this.resultsField = value;
				this.RaisePropertyChanged( "Results" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 2 )]
		public string Pages
		{
			get
			{
				return this.pagesField;
			}
			set
			{
				this.pagesField = value;
				this.RaisePropertyChanged( "Pages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public CorrectedQuery CorrectedQuery
		{
			get
			{
				return this.correctedQueryField;
			}
			set
			{
				this.correctedQueryField = value;
				this.RaisePropertyChanged( "CorrectedQuery" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "positiveInteger", Order = 4 )]
		public string RelevanceRank
		{
			get
			{
				return this.relevanceRankField;
			}
			set
			{
				this.relevanceRankField = value;
				this.RaisePropertyChanged( "RelevanceRank" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ASIN", Order = 5 )]
		public string[] ASIN
		{
			get
			{
				return this.aSINField;
			}
			set
			{
				this.aSINField = value;
				this.RaisePropertyChanged( "ASIN" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}