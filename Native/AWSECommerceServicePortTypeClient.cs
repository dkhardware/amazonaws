namespace Amazon.AWS.Native
{
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	public class AWSECommerceServicePortTypeClient : System.ServiceModel.ClientBase<AWSECommerceServicePortType>, AWSECommerceServicePortType
	{

		public AWSECommerceServicePortTypeClient()
		{
		}

		public AWSECommerceServicePortTypeClient( string endpointConfigurationName ) :
			base( endpointConfigurationName )
		{
		}

		public AWSECommerceServicePortTypeClient( string endpointConfigurationName, string remoteAddress ) :
			base( endpointConfigurationName, remoteAddress )
		{
		}

		public AWSECommerceServicePortTypeClient( string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress ) :
			base( endpointConfigurationName, remoteAddress )
		{
		}

		public AWSECommerceServicePortTypeClient( System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress ) :
			base( binding, remoteAddress )
		{
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		ItemSearchResponse1 AWSECommerceServicePortType.ItemSearch( ItemSearchRequest1 request )
		{
			return base.Channel.ItemSearch( request );
		}

		public ItemSearchResponse ItemSearch( ItemSearch ItemSearch1 )
		{
			ItemSearchRequest1 inValue = new ItemSearchRequest1();
			inValue.ItemSearch = ItemSearch1;
			ItemSearchResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).ItemSearch( inValue );
			return retVal.ItemSearchResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		ItemLookupResponse1 AWSECommerceServicePortType.ItemLookup( ItemLookupRequest1 request )
		{
			return base.Channel.ItemLookup( request );
		}

		public ItemLookupResponse ItemLookup( ItemLookup ItemLookup1 )
		{
			ItemLookupRequest1 inValue = new ItemLookupRequest1();
			inValue.ItemLookup = ItemLookup1;
			ItemLookupResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).ItemLookup( inValue );
			return retVal.ItemLookupResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		BrowseNodeLookupResponse1 AWSECommerceServicePortType.BrowseNodeLookup( BrowseNodeLookupRequest1 request )
		{
			return base.Channel.BrowseNodeLookup( request );
		}

		public BrowseNodeLookupResponse BrowseNodeLookup( BrowseNodeLookup BrowseNodeLookup1 )
		{
			BrowseNodeLookupRequest1 inValue = new BrowseNodeLookupRequest1();
			inValue.BrowseNodeLookup = BrowseNodeLookup1;
			BrowseNodeLookupResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).BrowseNodeLookup( inValue );
			return retVal.BrowseNodeLookupResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		SimilarityLookupResponse1 AWSECommerceServicePortType.SimilarityLookup( SimilarityLookupRequest1 request )
		{
			return base.Channel.SimilarityLookup( request );
		}

		public SimilarityLookupResponse SimilarityLookup( SimilarityLookup SimilarityLookup1 )
		{
			SimilarityLookupRequest1 inValue = new SimilarityLookupRequest1();
			inValue.SimilarityLookup = SimilarityLookup1;
			SimilarityLookupResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).SimilarityLookup( inValue );
			return retVal.SimilarityLookupResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		CartGetResponse1 AWSECommerceServicePortType.CartGet( CartGetRequest1 request )
		{
			return base.Channel.CartGet( request );
		}

		public CartGetResponse CartGet( CartGet CartGet1 )
		{
			CartGetRequest1 inValue = new CartGetRequest1();
			inValue.CartGet = CartGet1;
			CartGetResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).CartGet( inValue );
			return retVal.CartGetResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		CartAddResponse1 AWSECommerceServicePortType.CartAdd( CartAddRequest1 request )
		{
			return base.Channel.CartAdd( request );
		}

		public CartAddResponse CartAdd( CartAdd CartAdd1 )
		{
			CartAddRequest1 inValue = new CartAddRequest1();
			inValue.CartAdd = CartAdd1;
			CartAddResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).CartAdd( inValue );
			return retVal.CartAddResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		CartCreateResponse1 AWSECommerceServicePortType.CartCreate( CartCreateRequest1 request )
		{
			return base.Channel.CartCreate( request );
		}

		public CartCreateResponse CartCreate( CartCreate CartCreate1 )
		{
			CartCreateRequest1 inValue = new CartCreateRequest1();
			inValue.CartCreate = CartCreate1;
			CartCreateResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).CartCreate( inValue );
			return retVal.CartCreateResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		CartModifyResponse1 AWSECommerceServicePortType.CartModify( CartModifyRequest1 request )
		{
			return base.Channel.CartModify( request );
		}

		public CartModifyResponse CartModify( CartModify CartModify1 )
		{
			CartModifyRequest1 inValue = new CartModifyRequest1();
			inValue.CartModify = CartModify1;
			CartModifyResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).CartModify( inValue );
			return retVal.CartModifyResponse;
		}

		[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
		CartClearResponse1 AWSECommerceServicePortType.CartClear( CartClearRequest1 request )
		{
			return base.Channel.CartClear( request );
		}

		public CartClearResponse CartClear( CartClear CartClear1 )
		{
			CartClearRequest1 inValue = new CartClearRequest1();
			inValue.CartClear = CartClear1;
			CartClearResponse1 retVal = ( (AWSECommerceServicePortType)( this ) ).CartClear( inValue );
			return retVal.CartClearResponse;
		}
	}
}