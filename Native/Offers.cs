namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Offers : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string totalOffersField;

		private string totalOfferPagesField;

		private string moreOffersUrlField;

		private Offer[] offerField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 0 )]
		public string TotalOffers
		{
			get
			{
				return this.totalOffersField;
			}
			set
			{
				this.totalOffersField = value;
				this.RaisePropertyChanged( "TotalOffers" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 1 )]
		public string TotalOfferPages
		{
			get
			{
				return this.totalOfferPagesField;
			}
			set
			{
				this.totalOfferPagesField = value;
				this.RaisePropertyChanged( "TotalOfferPages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string MoreOffersUrl
		{
			get
			{
				return this.moreOffersUrlField;
			}
			set
			{
				this.moreOffersUrlField = value;
				this.RaisePropertyChanged( "MoreOffersUrl" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Offer", Order = 3 )]
		public Offer[] Offer
		{
			get
			{
				return this.offerField;
			}
			set
			{
				this.offerField = value;
				this.RaisePropertyChanged( "Offer" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}