namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Request : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string isValidField;

		private BrowseNodeLookupRequest browseNodeLookupRequestField;

		private ItemSearchRequest itemSearchRequestField;

		private ItemLookupRequest itemLookupRequestField;

		private SimilarityLookupRequest similarityLookupRequestField;

		private CartGetRequest cartGetRequestField;

		private CartAddRequest cartAddRequestField;

		private CartCreateRequest cartCreateRequestField;

		private CartModifyRequest cartModifyRequestField;

		private CartClearRequest cartClearRequestField;

		private ErrorsError[] errorsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string IsValid
		{
			get
			{
				return this.isValidField;
			}
			set
			{
				this.isValidField = value;
				this.RaisePropertyChanged( "IsValid" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public BrowseNodeLookupRequest BrowseNodeLookupRequest
		{
			get
			{
				return this.browseNodeLookupRequestField;
			}
			set
			{
				this.browseNodeLookupRequestField = value;
				this.RaisePropertyChanged( "BrowseNodeLookupRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public ItemSearchRequest ItemSearchRequest
		{
			get
			{
				return this.itemSearchRequestField;
			}
			set
			{
				this.itemSearchRequestField = value;
				this.RaisePropertyChanged( "ItemSearchRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public ItemLookupRequest ItemLookupRequest
		{
			get
			{
				return this.itemLookupRequestField;
			}
			set
			{
				this.itemLookupRequestField = value;
				this.RaisePropertyChanged( "ItemLookupRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public SimilarityLookupRequest SimilarityLookupRequest
		{
			get
			{
				return this.similarityLookupRequestField;
			}
			set
			{
				this.similarityLookupRequestField = value;
				this.RaisePropertyChanged( "SimilarityLookupRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public CartGetRequest CartGetRequest
		{
			get
			{
				return this.cartGetRequestField;
			}
			set
			{
				this.cartGetRequestField = value;
				this.RaisePropertyChanged( "CartGetRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public CartAddRequest CartAddRequest
		{
			get
			{
				return this.cartAddRequestField;
			}
			set
			{
				this.cartAddRequestField = value;
				this.RaisePropertyChanged( "CartAddRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public CartCreateRequest CartCreateRequest
		{
			get
			{
				return this.cartCreateRequestField;
			}
			set
			{
				this.cartCreateRequestField = value;
				this.RaisePropertyChanged( "CartCreateRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public CartModifyRequest CartModifyRequest
		{
			get
			{
				return this.cartModifyRequestField;
			}
			set
			{
				this.cartModifyRequestField = value;
				this.RaisePropertyChanged( "CartModifyRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 9 )]
		public CartClearRequest CartClearRequest
		{
			get
			{
				return this.cartClearRequestField;
			}
			set
			{
				this.cartClearRequestField = value;
				this.RaisePropertyChanged( "CartClearRequest" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 10 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Error", IsNullable = false )]
		public ErrorsError[] Errors
		{
			get
			{
				return this.errorsField;
			}
			set
			{
				this.errorsField = value;
				this.RaisePropertyChanged( "Errors" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}