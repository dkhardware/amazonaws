using System.Collections.Generic;

namespace Amazon.AWS.Native
{
	class ParamComparer : IComparer<string>
	{
		public int Compare( string p1, string p2 )
		{
			return string.CompareOrdinal( p1, p2 );
		}
	}
}