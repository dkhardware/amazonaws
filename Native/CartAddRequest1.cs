namespace Amazon.AWS.Native
{
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	[System.ComponentModel.EditorBrowsableAttribute( System.ComponentModel.EditorBrowsableState.Advanced )]
	[System.ServiceModel.MessageContractAttribute( IsWrapped = false )]
	public class CartAddRequest1
	{

		[System.ServiceModel.MessageBodyMemberAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01", Order = 0 )]
		public CartAdd CartAdd;

		public CartAddRequest1()
		{
		}

		public CartAddRequest1( CartAdd CartAdd )
		{
			this.CartAdd = CartAdd;
		}
	}
}