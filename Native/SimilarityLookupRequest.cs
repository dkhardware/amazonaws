namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class SimilarityLookupRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Condition conditionField;

		private bool conditionFieldSpecified;

		private string[] itemIdField;

		private string merchantIdField;

		private string[] responseGroupField;

		private SimilarityLookupRequestSimilarityType similarityTypeField;

		private bool similarityTypeFieldSpecified;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Condition Condition
		{
			get
			{
				return this.conditionField;
			}
			set
			{
				this.conditionField = value;
				this.RaisePropertyChanged( "Condition" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ConditionSpecified
		{
			get
			{
				return this.conditionFieldSpecified;
			}
			set
			{
				this.conditionFieldSpecified = value;
				this.RaisePropertyChanged( "ConditionSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ItemId", Order = 1 )]
		public string[] ItemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
				this.RaisePropertyChanged( "ItemId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string MerchantId
		{
			get
			{
				return this.merchantIdField;
			}
			set
			{
				this.merchantIdField = value;
				this.RaisePropertyChanged( "MerchantId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 3 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public SimilarityLookupRequestSimilarityType SimilarityType
		{
			get
			{
				return this.similarityTypeField;
			}
			set
			{
				this.similarityTypeField = value;
				this.RaisePropertyChanged( "SimilarityType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool SimilarityTypeSpecified
		{
			get
			{
				return this.similarityTypeFieldSpecified;
			}
			set
			{
				this.similarityTypeFieldSpecified = value;
				this.RaisePropertyChanged( "SimilarityTypeSpecified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}