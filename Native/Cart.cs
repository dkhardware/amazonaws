namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Cart : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Request requestField;

		private string cartIdField;

		private string hMACField;

		private string uRLEncodedHMACField;

		private string purchaseURLField;

		private string mobileCartURLField;

		private Price subTotalField;

		private CartItems cartItemsField;

		private SavedForLaterItems savedForLaterItemsField;

		private SimilarProductsSimilarProduct[] similarProductsField;

		private TopSellersTopSeller[] topSellersField;

		private NewReleasesNewRelease[] newReleasesField;

		private SimilarViewedProductsSimilarViewedProduct[] similarViewedProductsField;

		private OtherCategoriesSimilarProductsOtherCategoriesSimilarProduct[] otherCategoriesSimilarProductsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Request Request
		{
			get
			{
				return this.requestField;
			}
			set
			{
				this.requestField = value;
				this.RaisePropertyChanged( "Request" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string CartId
		{
			get
			{
				return this.cartIdField;
			}
			set
			{
				this.cartIdField = value;
				this.RaisePropertyChanged( "CartId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string HMAC
		{
			get
			{
				return this.hMACField;
			}
			set
			{
				this.hMACField = value;
				this.RaisePropertyChanged( "HMAC" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string URLEncodedHMAC
		{
			get
			{
				return this.uRLEncodedHMACField;
			}
			set
			{
				this.uRLEncodedHMACField = value;
				this.RaisePropertyChanged( "URLEncodedHMAC" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string PurchaseURL
		{
			get
			{
				return this.purchaseURLField;
			}
			set
			{
				this.purchaseURLField = value;
				this.RaisePropertyChanged( "PurchaseURL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string MobileCartURL
		{
			get
			{
				return this.mobileCartURLField;
			}
			set
			{
				this.mobileCartURLField = value;
				this.RaisePropertyChanged( "MobileCartURL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public Price SubTotal
		{
			get
			{
				return this.subTotalField;
			}
			set
			{
				this.subTotalField = value;
				this.RaisePropertyChanged( "SubTotal" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public CartItems CartItems
		{
			get
			{
				return this.cartItemsField;
			}
			set
			{
				this.cartItemsField = value;
				this.RaisePropertyChanged( "CartItems" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public SavedForLaterItems SavedForLaterItems
		{
			get
			{
				return this.savedForLaterItemsField;
			}
			set
			{
				this.savedForLaterItemsField = value;
				this.RaisePropertyChanged( "SavedForLaterItems" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 9 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "SimilarProduct", IsNullable = false )]
		public SimilarProductsSimilarProduct[] SimilarProducts
		{
			get
			{
				return this.similarProductsField;
			}
			set
			{
				this.similarProductsField = value;
				this.RaisePropertyChanged( "SimilarProducts" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 10 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "TopSeller", IsNullable = false )]
		public TopSellersTopSeller[] TopSellers
		{
			get
			{
				return this.topSellersField;
			}
			set
			{
				this.topSellersField = value;
				this.RaisePropertyChanged( "TopSellers" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 11 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "NewRelease", IsNullable = false )]
		public NewReleasesNewRelease[] NewReleases
		{
			get
			{
				return this.newReleasesField;
			}
			set
			{
				this.newReleasesField = value;
				this.RaisePropertyChanged( "NewReleases" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 12 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "SimilarViewedProduct", IsNullable = false )]
		public SimilarViewedProductsSimilarViewedProduct[] SimilarViewedProducts
		{
			get
			{
				return this.similarViewedProductsField;
			}
			set
			{
				this.similarViewedProductsField = value;
				this.RaisePropertyChanged( "SimilarViewedProducts" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 13 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "OtherCategoriesSimilarProduct", IsNullable = false )]
		public OtherCategoriesSimilarProductsOtherCategoriesSimilarProduct[] OtherCategoriesSimilarProducts
		{
			get
			{
				return this.otherCategoriesSimilarProductsField;
			}
			set
			{
				this.otherCategoriesSimilarProductsField = value;
				this.RaisePropertyChanged( "OtherCategoriesSimilarProducts" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}