namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CollectionsCollection : object, System.ComponentModel.INotifyPropertyChanged
	{

		private CollectionsCollectionCollectionSummary collectionSummaryField;

		private CollectionsCollectionCollectionParent collectionParentField;

		private CollectionsCollectionCollectionItem[] collectionItemField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public CollectionsCollectionCollectionSummary CollectionSummary
		{
			get
			{
				return this.collectionSummaryField;
			}
			set
			{
				this.collectionSummaryField = value;
				this.RaisePropertyChanged( "CollectionSummary" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public CollectionsCollectionCollectionParent CollectionParent
		{
			get
			{
				return this.collectionParentField;
			}
			set
			{
				this.collectionParentField = value;
				this.RaisePropertyChanged( "CollectionParent" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "CollectionItem", Order = 2 )]
		public CollectionsCollectionCollectionItem[] CollectionItem
		{
			get
			{
				return this.collectionItemField;
			}
			set
			{
				this.collectionItemField = value;
				this.RaisePropertyChanged( "CollectionItem" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}