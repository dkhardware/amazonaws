namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Bin : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string binNameField;

		private string binItemCountField;

		private BinBinParameter[] binParameterField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string BinName
		{
			get
			{
				return this.binNameField;
			}
			set
			{
				this.binNameField = value;
				this.RaisePropertyChanged( "BinName" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "positiveInteger", Order = 1 )]
		public string BinItemCount
		{
			get
			{
				return this.binItemCountField;
			}
			set
			{
				this.binItemCountField = value;
				this.RaisePropertyChanged( "BinItemCount" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "BinParameter", Order = 2 )]
		public BinBinParameter[] BinParameter
		{
			get
			{
				return this.binParameterField;
			}
			set
			{
				this.binParameterField = value;
				this.RaisePropertyChanged( "BinParameter" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}