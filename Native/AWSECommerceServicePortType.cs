namespace Amazon.AWS.Native
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	[System.ServiceModel.ServiceContractAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01", ConfigurationName = "AWS.AWSECommerceServicePortType" )]
	public interface AWSECommerceServicePortType
	{

		// CODEGEN: �������� ��������� ��������� � ��������� ItemSearch �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/ItemSearch", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		ItemSearchResponse1 ItemSearch( ItemSearchRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� ItemLookup �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/ItemLookup", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		ItemLookupResponse1 ItemLookup( ItemLookupRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� BrowseNodeLookup �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/BrowseNodeLookup", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		BrowseNodeLookupResponse1 BrowseNodeLookup( BrowseNodeLookupRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� SimilarityLookup �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/SimilarityLookup", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		SimilarityLookupResponse1 SimilarityLookup( SimilarityLookupRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� CartGet �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/CartGet", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		CartGetResponse1 CartGet( CartGetRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� CartAdd �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/CartAdd", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		CartAddResponse1 CartAdd( CartAddRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� CartCreate �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/CartCreate", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		CartCreateResponse1 CartCreate( CartCreateRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� CartModify �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/CartModify", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		CartModifyResponse1 CartModify( CartModifyRequest1 request );

		// CODEGEN: �������� ��������� ��������� � ��������� CartClear �� �������� �� RPC, �� ����������� ����������.
		[System.ServiceModel.OperationContractAttribute( Action = "http://soap.amazon.com/CartClear", ReplyAction = "*" )]
		[System.ServiceModel.XmlSerializerFormatAttribute( SupportFaults = true )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( VariationAttribute[] ) )]
		[System.ServiceModel.ServiceKnownTypeAttribute( typeof( Property[] ) )]
		CartClearResponse1 CartClear( CartClearRequest1 request );
	}
}