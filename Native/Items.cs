namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Items : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Request requestField;

		private CorrectedQuery correctedQueryField;

		private string qidField;

		private string engineQueryField;

		private string totalResultsField;

		private string totalPagesField;

		private string moreSearchResultsUrlField;

		private SearchResultsMapSearchIndex[] searchResultsMapField;

		private Item[] itemField;

		private SearchBinSet[] searchBinSetsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Request Request
		{
			get
			{
				return this.requestField;
			}
			set
			{
				this.requestField = value;
				this.RaisePropertyChanged( "Request" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public CorrectedQuery CorrectedQuery
		{
			get
			{
				return this.correctedQueryField;
			}
			set
			{
				this.correctedQueryField = value;
				this.RaisePropertyChanged( "CorrectedQuery" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string Qid
		{
			get
			{
				return this.qidField;
			}
			set
			{
				this.qidField = value;
				this.RaisePropertyChanged( "Qid" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string EngineQuery
		{
			get
			{
				return this.engineQueryField;
			}
			set
			{
				this.engineQueryField = value;
				this.RaisePropertyChanged( "EngineQuery" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 4 )]
		public string TotalResults
		{
			get
			{
				return this.totalResultsField;
			}
			set
			{
				this.totalResultsField = value;
				this.RaisePropertyChanged( "TotalResults" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 5 )]
		public string TotalPages
		{
			get
			{
				return this.totalPagesField;
			}
			set
			{
				this.totalPagesField = value;
				this.RaisePropertyChanged( "TotalPages" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public string MoreSearchResultsUrl
		{
			get
			{
				return this.moreSearchResultsUrlField;
			}
			set
			{
				this.moreSearchResultsUrlField = value;
				this.RaisePropertyChanged( "MoreSearchResultsUrl" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 7 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "SearchIndex", IsNullable = false )]
		public SearchResultsMapSearchIndex[] SearchResultsMap
		{
			get
			{
				return this.searchResultsMapField;
			}
			set
			{
				this.searchResultsMapField = value;
				this.RaisePropertyChanged( "SearchResultsMap" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Item", Order = 8 )]
		public Item[] Item
		{
			get
			{
				return this.itemField;
			}
			set
			{
				this.itemField = value;
				this.RaisePropertyChanged( "Item" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 9 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "SearchBinSet", IsNullable = false )]
		public SearchBinSet[] SearchBinSets
		{
			get
			{
				return this.searchBinSetsField;
			}
			set
			{
				this.searchBinSetsField = value;
				this.RaisePropertyChanged( "SearchBinSets" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}