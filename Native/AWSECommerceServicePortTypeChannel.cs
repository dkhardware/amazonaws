namespace Amazon.AWS.Native
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.ServiceModel", "4.0.0.0" )]
	public interface AWSECommerceServicePortTypeChannel : AWSECommerceServicePortType, System.ServiceModel.IClientChannel
	{
	}
}