namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Image : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string uRLField;

		private DecimalWithUnits heightField;

		private DecimalWithUnits widthField;

		private string isVerifiedField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string URL
		{
			get
			{
				return this.uRLField;
			}
			set
			{
				this.uRLField = value;
				this.RaisePropertyChanged( "URL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public DecimalWithUnits Height
		{
			get
			{
				return this.heightField;
			}
			set
			{
				this.heightField = value;
				this.RaisePropertyChanged( "Height" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public DecimalWithUnits Width
		{
			get
			{
				return this.widthField;
			}
			set
			{
				this.widthField = value;
				this.RaisePropertyChanged( "Width" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string IsVerified
		{
			get
			{
				return this.isVerifiedField;
			}
			set
			{
				this.isVerifiedField = value;
				this.RaisePropertyChanged( "IsVerified" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}