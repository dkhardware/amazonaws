namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class OfferListingAvailabilityAttributes : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string availabilityTypeField;

		private bool isPreorderField;

		private bool isPreorderFieldSpecified;

		private string minimumHoursField;

		private string maximumHoursField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string AvailabilityType
		{
			get
			{
				return this.availabilityTypeField;
			}
			set
			{
				this.availabilityTypeField = value;
				this.RaisePropertyChanged( "AvailabilityType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public bool IsPreorder
		{
			get
			{
				return this.isPreorderField;
			}
			set
			{
				this.isPreorderField = value;
				this.RaisePropertyChanged( "IsPreorder" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsPreorderSpecified
		{
			get
			{
				return this.isPreorderFieldSpecified;
			}
			set
			{
				this.isPreorderFieldSpecified = value;
				this.RaisePropertyChanged( "IsPreorderSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "integer", Order = 2 )]
		public string MinimumHours
		{
			get
			{
				return this.minimumHoursField;
			}
			set
			{
				this.minimumHoursField = value;
				this.RaisePropertyChanged( "MinimumHours" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "integer", Order = 3 )]
		public string MaximumHours
		{
			get
			{
				return this.maximumHoursField;
			}
			set
			{
				this.maximumHoursField = value;
				this.RaisePropertyChanged( "MaximumHours" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}