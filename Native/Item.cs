using System.IO;
using System.Text;
using System.Xml;

namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class Item : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string aSINField;

		private string parentASINField;

		private ErrorsError[] errorsField;

		private string detailPageURLField;

		private ItemLink[] itemLinksField;

		private string salesRankField;

		private Image smallImageField;

		private Image mediumImageField;

		private Image largeImageField;

		private ImageSet[] imageSetsField;

		private ItemAttributes itemAttributesField;

		private VariationAttribute[] variationAttributesField;

		private RelatedItems[] relatedItemsField;

		private CollectionsCollection[] collectionsField;

		private string[] subjectsField;

		private OfferSummary offerSummaryField;

		private Offers offersField;

		private VariationSummary variationSummaryField;

		private Variations variationsField;

		private CustomerReviews customerReviewsField;

		private EditorialReview[] editorialReviewsField;

		private SimilarProductsSimilarProduct[] similarProductsField;

		private AccessoriesAccessory[] accessoriesField;

		private TracksDisc[] tracksField;

		private BrowseNodes browseNodesField;

		private ItemAlternateVersion[] alternateVersionsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string ASIN
		{
			get
			{
				return this.aSINField;
			}
			set
			{
				this.aSINField = value;
				this.RaisePropertyChanged( "ASIN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string ParentASIN
		{
			get
			{
				return this.parentASINField;
			}
			set
			{
				this.parentASINField = value;
				this.RaisePropertyChanged( "ParentASIN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 2 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Error", IsNullable = false )]
		public ErrorsError[] Errors
		{
			get
			{
				return this.errorsField;
			}
			set
			{
				this.errorsField = value;
				this.RaisePropertyChanged( "Errors" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string DetailPageURL
		{
			get
			{
				return this.detailPageURLField;
			}
			set
			{
				this.detailPageURLField = value;
				this.RaisePropertyChanged( "DetailPageURL" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 4 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "ItemLink", IsNullable = false )]
		public ItemLink[] ItemLinks
		{
			get
			{
				return this.itemLinksField;
			}
			set
			{
				this.itemLinksField = value;
				this.RaisePropertyChanged( "ItemLinks" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string SalesRank
		{
			get
			{
				return this.salesRankField;
			}
			set
			{
				this.salesRankField = value;
				this.RaisePropertyChanged( "SalesRank" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 6 )]
		public Image SmallImage
		{
			get
			{
				return this.smallImageField;
			}
			set
			{
				this.smallImageField = value;
				this.RaisePropertyChanged( "SmallImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public Image MediumImage
		{
			get
			{
				return this.mediumImageField;
			}
			set
			{
				this.mediumImageField = value;
				this.RaisePropertyChanged( "MediumImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public Image LargeImage
		{
			get
			{
				return this.largeImageField;
			}
			set
			{
				this.largeImageField = value;
				this.RaisePropertyChanged( "LargeImage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 9 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "ImageSet", typeof( ImageSet ), IsNullable = false )]
		public ImageSet[] ImageSets
		{
			get
			{
				return this.imageSetsField;
			}
			set
			{
				this.imageSetsField = value;
				this.RaisePropertyChanged( "ImageSets" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 10 )]
		public ItemAttributes ItemAttributes
		{
			get
			{
				return this.itemAttributesField;
			}
			set
			{
				this.itemAttributesField = value;
				this.RaisePropertyChanged( "ItemAttributes" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 11 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "VariationAttribute", IsNullable = false )]
		public VariationAttribute[] VariationAttributes
		{
			get
			{
				return this.variationAttributesField;
			}
			set
			{
				this.variationAttributesField = value;
				this.RaisePropertyChanged( "VariationAttributes" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "RelatedItems", Order = 12 )]
		public RelatedItems[] RelatedItems
		{
			get
			{
				return this.relatedItemsField;
			}
			set
			{
				this.relatedItemsField = value;
				this.RaisePropertyChanged( "RelatedItems" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 13 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Collection", IsNullable = false )]
		public CollectionsCollection[] Collections
		{
			get
			{
				return this.collectionsField;
			}
			set
			{
				this.collectionsField = value;
				this.RaisePropertyChanged( "Collections" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 14 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Subject", IsNullable = false )]
		public string[] Subjects
		{
			get
			{
				return this.subjectsField;
			}
			set
			{
				this.subjectsField = value;
				this.RaisePropertyChanged( "Subjects" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 15 )]
		public OfferSummary OfferSummary
		{
			get
			{
				return this.offerSummaryField;
			}
			set
			{
				this.offerSummaryField = value;
				this.RaisePropertyChanged( "OfferSummary" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 16 )]
		public Offers Offers
		{
			get
			{
				return this.offersField;
			}
			set
			{
				this.offersField = value;
				this.RaisePropertyChanged( "Offers" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 17 )]
		public VariationSummary VariationSummary
		{
			get
			{
				return this.variationSummaryField;
			}
			set
			{
				this.variationSummaryField = value;
				this.RaisePropertyChanged( "VariationSummary" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 18 )]
		public Variations Variations
		{
			get
			{
				return this.variationsField;
			}
			set
			{
				this.variationsField = value;
				this.RaisePropertyChanged( "Variations" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 19 )]
		public CustomerReviews CustomerReviews
		{
			get
			{
				return this.customerReviewsField;
			}
			set
			{
				this.customerReviewsField = value;
				this.RaisePropertyChanged( "CustomerReviews" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 20 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "EditorialReview", IsNullable = false )]
		public EditorialReview[] EditorialReviews
		{
			get
			{
				return this.editorialReviewsField;
			}
			set
			{
				this.editorialReviewsField = value;
				this.RaisePropertyChanged( "EditorialReviews" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 21 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "SimilarProduct", IsNullable = false )]
		public SimilarProductsSimilarProduct[] SimilarProducts
		{
			get
			{
				return this.similarProductsField;
			}
			set
			{
				this.similarProductsField = value;
				this.RaisePropertyChanged( "SimilarProducts" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 22 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Accessory", IsNullable = false )]
		public AccessoriesAccessory[] Accessories
		{
			get
			{
				return this.accessoriesField;
			}
			set
			{
				this.accessoriesField = value;
				this.RaisePropertyChanged( "Accessories" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 23 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Disc", IsNullable = false )]
		public TracksDisc[] Tracks
		{
			get
			{
				return this.tracksField;
			}
			set
			{
				this.tracksField = value;
				this.RaisePropertyChanged( "Tracks" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 24 )]
		public BrowseNodes BrowseNodes
		{
			get
			{
				return this.browseNodesField;
			}
			set
			{
				this.browseNodesField = value;
				this.RaisePropertyChanged( "BrowseNodes" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 25 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "AlternateVersion", IsNullable = false )]
		public ItemAlternateVersion[] AlternateVersions
		{
			get
			{
				return this.alternateVersionsField;
			}
			set
			{
				this.alternateVersionsField = value;
				this.RaisePropertyChanged( "AlternateVersions" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}

		public string ToXml()
		{
			System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer( typeof( Item ) );

			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Encoding = new UnicodeEncoding( false, false ); // no BOM in a .NET string
			settings.Indent = true;
			settings.OmitXmlDeclaration = true;

			using ( StringWriter textWriter = new StringWriter() )
			{
				using ( XmlWriter xmlWriter = XmlWriter.Create( textWriter, settings ) )
				{
					serializer.Serialize( xmlWriter, this );
				}
				return textWriter.ToString(); //This is the output as a string
			}			
		}
	}
}