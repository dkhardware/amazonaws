namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class VariationSummary : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Price lowestPriceField;

		private Price highestPriceField;

		private Price lowestSalePriceField;

		private Price highestSalePriceField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Price LowestPrice
		{
			get
			{
				return this.lowestPriceField;
			}
			set
			{
				this.lowestPriceField = value;
				this.RaisePropertyChanged( "LowestPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Price HighestPrice
		{
			get
			{
				return this.highestPriceField;
			}
			set
			{
				this.highestPriceField = value;
				this.RaisePropertyChanged( "HighestPrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public Price LowestSalePrice
		{
			get
			{
				return this.lowestSalePriceField;
			}
			set
			{
				this.lowestSalePriceField = value;
				this.RaisePropertyChanged( "LowestSalePrice" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public Price HighestSalePrice
		{
			get
			{
				return this.highestSalePriceField;
			}
			set
			{
				this.highestSalePriceField = value;
				this.RaisePropertyChanged( "HighestSalePrice" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}