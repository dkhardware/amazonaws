namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class ItemAttributesItemDimensions : object, System.ComponentModel.INotifyPropertyChanged
	{

		private DecimalWithUnits heightField;

		private DecimalWithUnits lengthField;

		private DecimalWithUnits weightField;

		private DecimalWithUnits widthField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public DecimalWithUnits Height
		{
			get
			{
				return this.heightField;
			}
			set
			{
				this.heightField = value;
				this.RaisePropertyChanged( "Height" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public DecimalWithUnits Length
		{
			get
			{
				return this.lengthField;
			}
			set
			{
				this.lengthField = value;
				this.RaisePropertyChanged( "Length" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public DecimalWithUnits Weight
		{
			get
			{
				return this.weightField;
			}
			set
			{
				this.weightField = value;
				this.RaisePropertyChanged( "Weight" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public DecimalWithUnits Width
		{
			get
			{
				return this.widthField;
			}
			set
			{
				this.widthField = value;
				this.RaisePropertyChanged( "Width" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}