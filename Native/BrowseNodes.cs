namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class BrowseNodes : object, System.ComponentModel.INotifyPropertyChanged
	{

		private Request requestField;

		private BrowseNode[] browseNodeField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public Request Request
		{
			get
			{
				return this.requestField;
			}
			set
			{
				this.requestField = value;
				this.RaisePropertyChanged( "Request" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "BrowseNode", Order = 1 )]
		public BrowseNode[] BrowseNode
		{
			get
			{
				return this.browseNodeField;
			}
			set
			{
				this.browseNodeField = value;
				this.RaisePropertyChanged( "BrowseNode" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}