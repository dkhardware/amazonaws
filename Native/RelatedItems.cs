namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class RelatedItems : object, System.ComponentModel.INotifyPropertyChanged
	{

		private RelatedItemsRelationship relationshipField;

		private bool relationshipFieldSpecified;

		private string relationshipTypeField;

		private string relatedItemCountField;

		private string relatedItemPageCountField;

		private string relatedItemPageField;

		private RelatedItem[] relatedItemField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public RelatedItemsRelationship Relationship
		{
			get
			{
				return this.relationshipField;
			}
			set
			{
				this.relationshipField = value;
				this.RaisePropertyChanged( "Relationship" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool RelationshipSpecified
		{
			get
			{
				return this.relationshipFieldSpecified;
			}
			set
			{
				this.relationshipFieldSpecified = value;
				this.RaisePropertyChanged( "RelationshipSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string RelationshipType
		{
			get
			{
				return this.relationshipTypeField;
			}
			set
			{
				this.relationshipTypeField = value;
				this.RaisePropertyChanged( "RelationshipType" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 2 )]
		public string RelatedItemCount
		{
			get
			{
				return this.relatedItemCountField;
			}
			set
			{
				this.relatedItemCountField = value;
				this.RaisePropertyChanged( "RelatedItemCount" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 3 )]
		public string RelatedItemPageCount
		{
			get
			{
				return this.relatedItemPageCountField;
			}
			set
			{
				this.relatedItemPageCountField = value;
				this.RaisePropertyChanged( "RelatedItemPageCount" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 4 )]
		public string RelatedItemPage
		{
			get
			{
				return this.relatedItemPageField;
			}
			set
			{
				this.relatedItemPageField = value;
				this.RaisePropertyChanged( "RelatedItemPage" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "RelatedItem", Order = 5 )]
		public RelatedItem[] RelatedItem
		{
			get
			{
				return this.relatedItemField;
			}
			set
			{
				this.relatedItemField = value;
				this.RaisePropertyChanged( "RelatedItem" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}