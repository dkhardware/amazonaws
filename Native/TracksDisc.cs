namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class TracksDisc : object, System.ComponentModel.INotifyPropertyChanged
	{

		private TracksDiscTrack[] trackField;

		private string numberField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Track", Order = 0 )]
		public TracksDiscTrack[] Track
		{
			get
			{
				return this.trackField;
			}
			set
			{
				this.trackField = value;
				this.RaisePropertyChanged( "Track" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute( DataType = "positiveInteger" )]
		public string Number
		{
			get
			{
				return this.numberField;
			}
			set
			{
				this.numberField = value;
				this.RaisePropertyChanged( "Number" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}