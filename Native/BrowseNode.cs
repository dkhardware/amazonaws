namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class BrowseNode : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string browseNodeIdField;

		private string nameField;

		private bool isCategoryRootField;

		private bool isCategoryRootFieldSpecified;

		private Property[] propertiesField;

		private BrowseNode[] childrenField;

		private BrowseNode[] ancestorsField;

		private TopSellersTopSeller[] topSellersField;

		private NewReleasesNewRelease[] newReleasesField;

		private TopItemSet[] topItemSetField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string BrowseNodeId
		{
			get
			{
				return this.browseNodeIdField;
			}
			set
			{
				this.browseNodeIdField = value;
				this.RaisePropertyChanged( "BrowseNodeId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string Name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
				this.RaisePropertyChanged( "Name" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public bool IsCategoryRoot
		{
			get
			{
				return this.isCategoryRootField;
			}
			set
			{
				this.isCategoryRootField = value;
				this.RaisePropertyChanged( "IsCategoryRoot" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsCategoryRootSpecified
		{
			get
			{
				return this.isCategoryRootFieldSpecified;
			}
			set
			{
				this.isCategoryRootFieldSpecified = value;
				this.RaisePropertyChanged( "IsCategoryRootSpecified" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 3 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "Property", IsNullable = false )]
		public Property[] Properties
		{
			get
			{
				return this.propertiesField;
			}
			set
			{
				this.propertiesField = value;
				this.RaisePropertyChanged( "Properties" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 4 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "BrowseNode", IsNullable = false )]
		public BrowseNode[] Children
		{
			get
			{
				return this.childrenField;
			}
			set
			{
				this.childrenField = value;
				this.RaisePropertyChanged( "Children" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 5 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "BrowseNode", IsNullable = false )]
		public BrowseNode[] Ancestors
		{
			get
			{
				return this.ancestorsField;
			}
			set
			{
				this.ancestorsField = value;
				this.RaisePropertyChanged( "Ancestors" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 6 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "TopSeller", IsNullable = false )]
		public TopSellersTopSeller[] TopSellers
		{
			get
			{
				return this.topSellersField;
			}
			set
			{
				this.topSellersField = value;
				this.RaisePropertyChanged( "TopSellers" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 7 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "NewRelease", IsNullable = false )]
		public NewReleasesNewRelease[] NewReleases
		{
			get
			{
				return this.newReleasesField;
			}
			set
			{
				this.newReleasesField = value;
				this.RaisePropertyChanged( "NewReleases" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "TopItemSet", Order = 8 )]
		public TopItemSet[] TopItemSet
		{
			get
			{
				return this.topItemSetField;
			}
			set
			{
				this.topItemSetField = value;
				this.RaisePropertyChanged( "TopItemSet" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}