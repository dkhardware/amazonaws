namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartAdd : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string marketplaceDomainField;

		private string aWSAccessKeyIdField;

		private string associateTagField;

		private string validateField;

		private string xMLEscapingField;

		private CartAddRequest sharedField;

		private CartAddRequest[] requestField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string MarketplaceDomain
		{
			get
			{
				return this.marketplaceDomainField;
			}
			set
			{
				this.marketplaceDomainField = value;
				this.RaisePropertyChanged( "MarketplaceDomain" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string AWSAccessKeyId
		{
			get
			{
				return this.aWSAccessKeyIdField;
			}
			set
			{
				this.aWSAccessKeyIdField = value;
				this.RaisePropertyChanged( "AWSAccessKeyId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string AssociateTag
		{
			get
			{
				return this.associateTagField;
			}
			set
			{
				this.associateTagField = value;
				this.RaisePropertyChanged( "AssociateTag" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string Validate
		{
			get
			{
				return this.validateField;
			}
			set
			{
				this.validateField = value;
				this.RaisePropertyChanged( "Validate" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string XMLEscaping
		{
			get
			{
				return this.xMLEscapingField;
			}
			set
			{
				this.xMLEscapingField = value;
				this.RaisePropertyChanged( "XMLEscaping" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public CartAddRequest Shared
		{
			get
			{
				return this.sharedField;
			}
			set
			{
				this.sharedField = value;
				this.RaisePropertyChanged( "Shared" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "Request", Order = 6 )]
		public CartAddRequest[] Request
		{
			get
			{
				return this.requestField;
			}
			set
			{
				this.requestField = value;
				this.RaisePropertyChanged( "Request" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}