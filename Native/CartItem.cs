namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class CartItem : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string cartItemIdField;

		private string aSINField;

		private string sellerNicknameField;

		private string quantityField;

		private string titleField;

		private string productGroupField;

		private CartItemKeyValuePair[] metaDataField;

		private Price priceField;

		private Price itemTotalField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 0 )]
		public string CartItemId
		{
			get
			{
				return this.cartItemIdField;
			}
			set
			{
				this.cartItemIdField = value;
				this.RaisePropertyChanged( "CartItemId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public string ASIN
		{
			get
			{
				return this.aSINField;
			}
			set
			{
				this.aSINField = value;
				this.RaisePropertyChanged( "ASIN" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 2 )]
		public string SellerNickname
		{
			get
			{
				return this.sellerNicknameField;
			}
			set
			{
				this.sellerNicknameField = value;
				this.RaisePropertyChanged( "SellerNickname" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 3 )]
		public string Quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
				this.RaisePropertyChanged( "Quantity" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 4 )]
		public string Title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
				this.RaisePropertyChanged( "Title" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 5 )]
		public string ProductGroup
		{
			get
			{
				return this.productGroupField;
			}
			set
			{
				this.productGroupField = value;
				this.RaisePropertyChanged( "ProductGroup" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayAttribute( Order = 6 )]
		[System.Xml.Serialization.XmlArrayItemAttribute( "KeyValuePair", IsNullable = false )]
		public CartItemKeyValuePair[] MetaData
		{
			get
			{
				return this.metaDataField;
			}
			set
			{
				this.metaDataField = value;
				this.RaisePropertyChanged( "MetaData" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 7 )]
		public Price Price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
				this.RaisePropertyChanged( "Price" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 8 )]
		public Price ItemTotal
		{
			get
			{
				return this.itemTotalField;
			}
			set
			{
				this.itemTotalField = value;
				this.RaisePropertyChanged( "ItemTotal" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}