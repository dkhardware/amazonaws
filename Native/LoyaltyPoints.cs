namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( AnonymousType = true, Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class LoyaltyPoints : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string pointsField;

		private Price typicalRedemptionValueField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( DataType = "nonNegativeInteger", Order = 0 )]
		public string Points
		{
			get
			{
				return this.pointsField;
			}
			set
			{
				this.pointsField = value;
				this.RaisePropertyChanged( "Points" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( Order = 1 )]
		public Price TypicalRedemptionValue
		{
			get
			{
				return this.typicalRedemptionValueField;
			}
			set
			{
				this.typicalRedemptionValueField = value;
				this.RaisePropertyChanged( "TypicalRedemptionValue" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}