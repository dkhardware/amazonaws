namespace Amazon.AWS.Native
{
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Xml", "4.0.30319.34234" )]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute( "code" )]
	[System.Xml.Serialization.XmlTypeAttribute( Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01" )]
	public class BrowseNodeLookupRequest : object, System.ComponentModel.INotifyPropertyChanged
	{

		private string[] browseNodeIdField;

		private string[] responseGroupField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "BrowseNodeId", Order = 0 )]
		public string[] BrowseNodeId
		{
			get
			{
				return this.browseNodeIdField;
			}
			set
			{
				this.browseNodeIdField = value;
				this.RaisePropertyChanged( "BrowseNodeId" );
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute( "ResponseGroup", Order = 1 )]
		public string[] ResponseGroup
		{
			get
			{
				return this.responseGroupField;
			}
			set
			{
				this.responseGroupField = value;
				this.RaisePropertyChanged( "ResponseGroup" );
			}
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged( string propertyName )
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ( ( propertyChanged != null ) )
			{
				propertyChanged( this, new System.ComponentModel.PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}