﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Globalization;

namespace Amazon.AWS
{

	public class DictionaryStorage : Dictionary<string, string>, INotifyPropertyChanged
	{
		public DictionaryStorage()
		{
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public DictionaryStorage( IDictionary<string, string> dictionary )
			: base( dictionary )
		{
		}

		protected T _Get2<T>( string key, T defaultValue = default(T) ) where T : IConvertible
		{
			if ( !ContainsKey( key ) )
				return defaultValue;
			try
			{
				return (T)Convert.ChangeType( this[key], typeof( T ), CultureInfo.InvariantCulture );
			}
			catch
			{
				return defaultValue;
			}
		}

		protected virtual T _Get<T>( T defaultValue, [CallerMemberName] string name = null ) where T : IConvertible
		{
			if ( String.IsNullOrEmpty( name ) )
				name = new StackFrame( 1 ).GetMethod().Name;

			var key = ( name.StartsWith( "get_" ) ) ? name.Substring( 4 ) : name;

			return _Get2( key, defaultValue );
		}

		protected void _Set2<T>( string key, T value )
		{
			var converted = (string)Convert.ChangeType( value, typeof( string ), CultureInfo.InvariantCulture );
			var propertyChanged = true;
			if ( base.ContainsKey( key ) )
				propertyChanged = ( base[key] != converted );

			this[key] = converted;
			if ( propertyChanged && PropertyChanged != null )
				PropertyChanged( this, new PropertyChangedEventArgs( key ) );
		}

		protected void _Set<T>( T value, [CallerMemberName] string name = null )
		{
			if ( String.IsNullOrEmpty( name ) )
				name = new StackFrame( 1 ).GetMethod().Name;

			var key = ( name.StartsWith( "set_" ) ) ? name.Substring( 4 ) : name;
			_Set2( key, value );

		}

		public void Forget<TThis, T>( Expression<Func<TThis, T>> property )
		{
			var propertyInfo = ( (MemberExpression)property.Body ).Member as PropertyInfo;
			if ( propertyInfo == null )
			{
				throw new ArgumentException( "The lambda expression 'property' should point to a valid Property" );
			}

			var name = propertyInfo.Name;
			var key = ( name.StartsWith( "get_" ) ) ? name.Substring( 4 ) : name;
			Remove( key );

		}

		public void Forget<T>( Expression<Func<T>> property )
		{
			var propertyInfo = ( (MemberExpression)property.Body ).Member as PropertyInfo;
			if ( propertyInfo == null )
			{
				throw new ArgumentException( "The lambda expression 'property' should point to a valid Property" );
			}

			var name = propertyInfo.Name;
			var key = ( name.StartsWith( "get_" ) ) ? name.Substring( 4 ) : name;
			Remove( key );

		}

	}

	public class AwsDictionary : DictionaryStorage
	{
		public AwsDictionary( IDictionary<string, string> dictionary ) : base( dictionary )
		{
		}

		public AwsDictionary( AwsDictionary owner, AwsDictionary parent ) : base( owner ?? new Dictionary<string, string>() )
		{
			Parent = parent;
		}

		public AwsDictionary Parent { get; private set; }

		public AwsDictionary ParentOrSelf
		{
			get { return Parent ?? this; }
		}

		public int ItemPage
		{
			get { return _Get(0); }
			set { _Set( value ); }
		}

		public string Sort
		{
			get { return _Get("price"); }
			set { _Set( value  ); }
		}


		public string SearchIndex
		{
			get { return _Get( "all" ); }
			set { _Set( value ); }
		}

		public string Manufacturer
		{
			get { return _Get( "" ); }
			set { _Set( value ); }
		}

		public Int64 MinimumPrice
		{
			get
			{
				return _Get( 0L );
			}
			set
			{
				_Set( value );
			}
		}

		public Int64 MaximumPrice
		{
			get
			{
				return _Get( Int64.MaxValue );
			}
			set
			{
				_Set(value);
			}
		}

		public AwsDictionary()
		{
		}

		public AwsDictionary Clone()
		{
			return new AwsDictionary(this, this.Parent);
		}

		protected override T _Get<T>( T defaultValue, [CallerMemberName] string name = null )
		{
			if ( String.IsNullOrEmpty( name ) )
				name = new StackFrame( 1 ).GetMethod().Name;

			var key = ( name.StartsWith( "get_" ) ) ? name.Substring( 4 ) : name;

			if ( !ContainsKey( key ) && Parent != null )
				return Parent._Get2<T>( key, defaultValue );

			return _Get2( key, defaultValue);
		}

		public void Merge( AwsDictionary master )
		{
			foreach ( var kvp in master.Where( kvp => !ContainsKey( kvp.Key ) ) )
			{
				Add( kvp.Key, kvp.Value  );
			}
		}

		public IDictionary<string, string> Flatten()
		{
			var d = new Dictionary<string, string>( this );
			if ( this.Parent != null )
			{
				var fd = Parent.Flatten();
				foreach ( var kvp in fd.Where( kvp => !d.ContainsKey( kvp.Key ) ) )
				{
					d.Add( kvp.Key, kvp.Value );
				}
			}
			return d;
		}
		public void Forget<T>( Expression<Func<AwsDictionary, T>> property )
		{
			var propertyInfo = ( (MemberExpression)property.Body ).Member as PropertyInfo;
			if ( propertyInfo == null )
			{
				throw new ArgumentException( "The lambda expression 'property' should point to a valid Property" );
			}

			var name = propertyInfo.Name;
			var key = ( name.StartsWith( "get_" ) ) ? name.Substring( 4 ) : name;
			Remove( key );

		}

	}
}
